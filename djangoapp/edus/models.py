from django.db import models
from django.template.defaultfilters import slugify 

class Subject(models.Model):
  subject_name = models.CharField(max_length=100, unique=True)
  subject_slug = models.SlugField(max_length=100, unique=True)
  description = models.CharField(max_length=100, blank=True)
  created_at = models.DateTimeField(auto_now_add=True)
  
  def __str__(self):
    return self.subject_name 
  
  def save(self, *args, **kwargs):
    if not self.subject_slug:
      self.subject_slug = slugify(self.subject_name)
    return super().save(*args, **kwargs)


class SubSubject(models.Model):
  name=models.CharField(max_length=100, unique=True)
  slug=models.SlugField(max_length=100, unique=True)
  description=models.CharField(max_length=100, blank=True)
  created_at=models.DateTimeField(auto_now_add=True)
  subject=models.ForeignKey(Subject, on_delete=models.CASCADE)
  
  def __str__(self):
    return self.name
  
  def save(self, *args, **kwargs):
    if not self.slug:
      self.slug=slugify(self.name)
    return super().save(*args, **kwargs)
  
  
  
class Topic(models.Model):
  topic_name = models.CharField(max_length=100, unique=True)
  topic_slug = models.SlugField(max_length=50, unique=True)
  article = models.TextField(blank=True)
  created_at = models.DateTimeField(auto_now_add=True)
  sub_subject = models.ForeignKey(SubSubject, on_delete=models.CASCADE)
  
  
  def __str__(self):
    return self.topic_name 
  
  def save(self, *args, **kwargs):
    if not self.topic_slug:
      self.topic_slug = slugify(self.topic_name)
    return super().save(*args, **kwargs)
  

  
  
  
