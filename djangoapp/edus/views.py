from django.shortcuts import render
from .models import Subject, Topic, SubSubject
from .serializers import SubjectSerializer, TopicSerializer, SubSubjectSerializer 
from django.http import Http404 
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status 
from rest_framework import authentication, permissions
from django.db.models import Q

"""FOR SUBJECT"""
class SubjectList(APIView):
    """
    List all subjects, or create a new snippet.
    """
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    def get(self, request, format=None):
        subjects = Subject.objects.all()
        serializer = SubjectSerializer(subjects, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = SubjectSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else: 
            print(serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
      
class SubjectDetail(APIView):
    """
    Retrieve, update or delete a subject instance.
    """
    def get_object(self, pk):
        try:
            return Subject.objects.get(pk=pk)
        except Subject.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        subject = self.get_object(pk)
        serializer = SubjectSerializer(subject)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        subject = self.get_object(pk)
        serializer = SubjectSerializer(subject, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def patch(self, request, pk, format=None):
        subject = self.get_object(pk)
        serializer = SubjectSerializer(subject, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        subject = self.get_object(pk)
        subject.delete()

        return Response({'id': pk}, status=status.HTTP_200_OK)



"""FOR SUBSUBJECT"""
class SubSubjectList(APIView):
    """
    List all sub-topics, or create a new snippet.
    """
    
    def get(self, request, format=None):
        str_id = request.query_params.get('id') 
        id = int(str_id)
        subSubject = SubSubject.objects.filter(subject=id)
        serializer = SubSubjectSerializer(subSubject, many=True)
        return Response(serializer.data)
    

    def post(self, request, format=None):
        serializer = SubSubjectSerializer(data=request.data)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

      
class SubSubjectDetail(APIView):
    """
    Retrieve, update or delete a sub topic instance.
    """
    def get_object(self, pk):
        try:
            return SubSubject.objects.get(pk=pk)
        except SubSubject.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        topic = self.get_object(pk)
        serializer = SubSubjectSerializer(topic)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        print("PUT request received")
        topic = self.get_object(pk)
        serializer = SubSubjectSerializer(topic, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def patch(self, request, pk, format=None):
        topic = self.get_object(pk)
        serializer = SubSubjectSerializer(topic, data=request.data, partial=True) # Notice the partial=True
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        subSubject = self.get_object(pk)
        subSubject.delete()
        return Response({'id': pk}, status=status.HTTP_200_OK)



""" FOR TOPICS """      
class TopicList(APIView):
    """
    List all topics, or create a new snippet.
    """
    
    def get(self, request, format=None):
        str_id = request.query_params.get('id')
        id = int(str_id)
        topics = Topic.objects.filter(Q(sub_subject=id))
        serializer = TopicSerializer(topics, many=True)
        return Response(serializer.data)
    

    def post(self, request, format=None):
        serializer = TopicSerializer(data=request.data)
        if serializer.is_valid(): 
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
      
      
class TopicDetail(APIView):
    """
    Retrieve, update or delete a topic instance.
    """
    def get_object(self, pk):
        try:
            return Topic.objects.get(pk=pk)
        except Topic.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        topic = self.get_object(pk)
        serializer = TopicSerializer(topic)
        
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        print("PUT request received")
        topic = self.get_object(pk)
        serializer = TopicSerializer(topic, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def patch(self, request, pk, format=None):
        topic = self.get_object(pk)
        serializer = TopicSerializer(topic, data=request.data, partial=True) 
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        topic = self.get_object(pk)
        topic.delete()
        return Response({'id': pk}, status=status.HTTP_200_OK)
    

    def head(self, request, *args, **kwargs):
        self.get(request, *args, **kwargs)  
        return Response(status=status.HTTP_200_OK, headers={'Custom-Header': 'Value'})
    

    def options(self, request, *args, **kwargs):
        data = {
            'name': 'Topic Detail API',
            'description': 'API endpoint that allows topics to be viewed or edited.',
            'renders': [renderer.media_type for renderer in self.renderer_classes],
            'parses': [parser.media_type for parser in self.parser_classes],
        }
        return Response(data, status=status.HTTP_200_OK)
    

      
      

