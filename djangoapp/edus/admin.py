from django.contrib import admin
from .models import Subject, Topic, SubSubject

class SubjectAdmin(admin.ModelAdmin):
  list_display = ('id', 'subject_name', 'subject_slug', 'description', 'created_at')
  prepopulated_fields = {'subject_slug': ('subject_name',)}
  
  
class TopicAdmin(admin.ModelAdmin):
  list_display = ('id', 'topic_name', 'topic_slug', 'sub_subject')
  prepopulated_fields = {'topic_slug': ('topic_name',)}
  search_fields = ('topic_name',)
  
class SubSubjectAdmin(admin.ModelAdmin):
  list_display = ('id', 'name', 'slug', 'created_at', 'subject')
  prepopulated_fields = {'slug': ('name',)}
  

admin.site.register(Subject, SubjectAdmin)
admin.site.register(Topic, TopicAdmin)
admin.site.register(SubSubject, SubSubjectAdmin)  

