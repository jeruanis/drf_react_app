from rest_framework import serializers
from .models import Subject, Topic, SubSubject 


class SubjectSerializer(serializers.ModelSerializer):
    subject_slug = serializers.SlugField(required=False)
    class Meta:
      model = Subject 
      fields = '__all__'  
   
  
class TopicSerializer(serializers.ModelSerializer):
  topic_slug = serializers.SlugField(required=False)
  subjectId = serializers.SerializerMethodField()
  class Meta:
    model = Topic 
    fields = '__all__'
    
  def get_subjectId(self, obj):
      if obj.sub_subject and obj.sub_subject.subject:
          return obj.sub_subject.subject.id
      return None
    
class SubSubjectSerializer(serializers.ModelSerializer):
  slug=serializers.SlugField(required=False)
  class Meta:
    model = SubSubject 
    fields = '__all__'
  
  
  
