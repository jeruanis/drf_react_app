from django.urls import path 
from rest_framework.urlpatterns import format_suffix_patterns 
from . import views

app_name = 'edus'
urlpatterns = [
  path('subjects/', views.SubjectList.as_view(), name='subject-list'),
  path('<int:pk>/', views.SubjectDetail.as_view(), name='subject-detail'), 
  path('topics/', views.TopicList.as_view(), name='topic-list'),
  path('detail/<int:pk>/', views.TopicDetail.as_view(), name='topic-detail'),
  path('topic_search/', views.TopicSearch.as_view(), name='topic-search'),
  path('sub-subject/', views.SubSubjectList.as_view(), name='sub-subject-listp'),
  path('sub-subject/<int:pk>', views.SubSubjectList.as_view(), name='sub-subject-listg'),
  path('sub-subject/detail/<int:pk>', views.SubSubjectDetail.as_view(), name='sub-subject-detail')
]

""" The code below urlpatterns is for content-type negotiation when doing API and when doing regular django this is not needed"""
urlpatterns = format_suffix_patterns(urlpatterns)
 