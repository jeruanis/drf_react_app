from django.shortcuts import render, redirect
from .models import Account
from django.contrib import messages, auth
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
import json
from django.utils.text import slugify
import uuid
from datetime import datetime, timedelta
from django.utils import timezone

from rest_framework import status 
from rest_framework.decorators import api_view, permission_classes, authentication_classes 
from rest_framework.response import Response 
from .serializers  import AuthorizationSerializer, AuthorizationSerializerOAuth
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authtoken.models import Token  # if you are using token-based authentication
from rest_framework.authentication import TokenAuthentication


def generate_unique_username(first_name):
    base_username = slugify(first_name)
    username = base_username
    suffix = 1

    while Account.objects.filter(username=username).exists():
        username = f"{base_username}-{suffix}"
        suffix += 1
        
    return username
 

@api_view(['POST'])
def signup(request):
  if request.method == "POST":
    try:
        serializerOAuth = AuthorizationSerializerOAuth(data=request.data)
        serializer = AuthorizationSerializer(data=request.data) 
        if serializerOAuth.is_valid():
          user = serializerOAuth.save()
          token, created = Token.objects.get_or_create(user=user)
          username = user.username
          user_token = token.key
          email = user.email
          
          return Response({
              'status': 200,
              'username': username,
              'token': user_token,
              'google': 'goog',
              'imageUrl': user.profile_image if user.profile_image else 'imageUrl',
              'email': email
          }, status=status.HTTP_200_OK) 

       
        elif serializer.is_valid():  
            
          user = serializer.save() 
          token, created = Token.objects.get_or_create(user=user)
          username = user.username  
          user_token = token.key 
          email=user.email 
          
          return Response({
            'status':200, 
            'username': username, 
            'token': user_token, 
            'gologin':'login', 
            'imageUrl':user.profile_image if user.profile_image else 'imageUrl', 
            'email': email
          })
          
        else:
          return Response({'Error': 'Invalid credentials'}, status=status.HTTP_400_BAD_REQUEST)
        

    except ValidationError as e:
        return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
    except ValueError as e:
        return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
      return Response({"error": f"An unexpected error occurred: {str(e)}"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

  else:
    return Response({'detail': 'Method Not Allowed'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
    

@api_view(['POST'])
@permission_classes((AllowAny,))
def login(request): 
  if request.method == 'POST':
    try:
      email = request.data.get('email')
      password = request.data.get('password')
      
      if email is None or password is None:
        return Response({'Error': 'Please provide both email and password'}, status=status.HTTP_400_BAD_REQUEST)
      
      user = authenticate(username=email, password=password)
      
      if user:
          try:
            token, create= Token.objects.get_or_create(user=user)
            return Response({'token': token.key, 'username':user.username, 'imageUrl':user.profile_image if user.profile_image else 'imageUrl', 'tohome':'home'}, status=status.HTTP_200_OK)
          
          except Token.DoesNotExist:
            return Response({'Error': 'Token does not exist.'}, status=status.HTTP_400_BAD_REQUEST)
          
      else:
          return Response({'Error': 'Invalid credentials'}, status=status.HTTP_400_BAD_REQUEST)
        
    except ValidationError as e:
      return Response(request, str(e), status=status.HTTP_400_BAD_REQUEST)
    except ValueError as e:
      return Response(request, str(e), status=status.HTTP_400_BAD_REQUEST)
  else:
    return Response({"Error" : "Method Error"}, status=status.HTTP_405_METHOD_NOT_ALLOWED)
  

@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated,])
def loginkeep(request): 
  user = request.user
  return Response({
    'token': user.auth_token.key if hasattr(user, 'auth_token') else None,
    'username': user.username,
    'imageUrl': user.profile_image if user.profile_image else 'imageUrl',
    'tohome': 'home'
  })
    
     
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated,])
def logout(request):
  request.auth.delete()
  return Response({"detail": "logoutSuccess"})


          
    
  
