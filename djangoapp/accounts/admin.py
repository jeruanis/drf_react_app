from django.contrib.admin.models import LogEntry
from django.contrib import admin
from .models import Account

admin.site.register(Account)
admin.site.register(LogEntry)