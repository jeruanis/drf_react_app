from rest_framework import serializers
from .models import Account 
from django.core.validators import RegexValidator 
from django.utils.text import slugify
from datetime import datetime, timedelta

def generate_unique_username(first_name):
    # Generate a unique username based on the first_name and add a number suffix if needed
    base_username = slugify(first_name)
    username = base_username
    suffix = 1

    while Account.objects.filter(username=username).exists():
        # If the username already exists, append a number suffix and try again
        username = f"{base_username}-{suffix}"
        suffix += 1
        
    return username

# define custom validations
lfnameVal = RegexValidator(r'^[A-Za-z\s]{2,25}$', 'Only letters, spaces, and from 2 to 25 characters are allowed.')
emailVal = RegexValidator(r'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$', 'Invalid Email Format.')
passwordVal = RegexValidator(r'^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{5,30}$', 'Password should be 5 to 30 characters with at least one letter, one number, and one special character.') 

# using same naming convention with react payload
class AuthorizationSerializerOAuth(serializers.ModelSerializer):
  
  # Since not all the field will be used define each one
  uniqid = serializers.CharField()
  first_name = serializers.CharField()
  last_name = serializers.CharField()
  email = serializers.CharField()
  profile_image = serializers.CharField()
    
  class Meta:
    model = Account
    fields = ('uniqid', 'first_name', 'last_name', 'email', 'profile_image')
    
  def create(self, validated_data):
    uniqid = validated_data.pop('uniqid', None) # pop from create_user
    profile_image = validated_data.pop('profile_image', None) #  pop from create_user
    password = ''

    isUniqIdExisting = Account.objects.filter(uniqid=uniqid).exists()
    if not isUniqIdExisting:
        # username = generate_unique_username(first_name)
        user = Account.objects.create_user(
          **validated_data, 
          password=password
          )
        user.username = generate_unique_username(user.first_name)
        user.uniqid = uniqid
        user.profile_image = profile_image
        #Note that the above are just saved in the memory below will be saved in the database
        user.save()

    else:
        user = Account.objects.get(uniqid=uniqid)

    return user
      
       
# Using different naming convention with react payload
class AuthorizationSerializer(serializers.ModelSerializer):
    
    # Since not all the field will be used and with different naming convention; define each one
    # The source is referencing to the models naming convention
    name = serializers.CharField(validators=[lfnameVal], source='first_name')
    lastName = serializers.CharField(validators=[lfnameVal], source='last_name')
    confirmEmail = serializers.CharField(write_only=True)
    pwd = serializers.CharField(write_only=True, validators=[passwordVal], source='password')
    matchPwd = serializers.CharField(write_only=True)

    class Meta:
        model = Account
        #put only the fields present in the model or in the custom model backend if any
        fields = ('name', 'lastName', 'email', 'confirmEmail', 'pwd',  'matchPwd', 'uniqid')

    #make data validation for email and password matching
    def validate(self, data):
        # Check if email matches confirmEmail
        if data['email'] != data['confirmEmail']:
            raise serializers.ValidationError({"confirmEmail": "Emails must match."})
        
        # Check if password matches matchPwd
        if data['password'] != data['matchPwd']:  
            raise serializers.ValidationError({"matchPwd": "Passwords must match."})

        return data

    
    def create(self, validated_data):
      # Remove/pop fields that are not on the model since they are included in the field option
      validated_data.pop('confirmEmail', None)
      validated_data.pop('matchPwd', None)

      # Extracting required parameters from the react payload; Should follow model naming convention
      first_name = validated_data.pop('first_name')
      last_name = validated_data.pop('last_name')
      email = validated_data.pop('email')
      password = validated_data.pop('password')
    
      # Use custom manager's create_user method
      user = Account.objects.create_user(
          first_name=first_name,
          last_name=last_name,
          email=email,
          password=password,
          **validated_data # use .pop() and not .get() for duplicated items
      )
      user.username = generate_unique_username(user.first_name)
      user.uniqid = user.username
      user.save()

      return user
    
# 
    
  


    