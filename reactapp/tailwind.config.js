/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      height: {
        '128': '31rem',
        'screen-90': '93vh',
        'cust55':'35%'
      }, 
      gridTemplateRows: {
        '[auto,auto,1fr]': 'auto auto 1fr',
      },
     },
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
  ], 
} 