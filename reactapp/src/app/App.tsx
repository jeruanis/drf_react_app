import React, { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import Signup from '../features/signup/SignupPage'
import Login from '../features/login/LoginPage'
// import Home from '../components/home'
import NavComponent from '../components/NavBar';
import Logout from '../features/logout/logout';
import ErrorBoundary from './ErrorBoundary';
import AddSubjectForm from '../components/AddSubjectForm';
import AddTopicForm from '../components/AddTopicForm';
import AddSubSubjectForm from '../components/AddSubSubjectForm';
import Detail from '../components/Detail';
import Courses from '../components/Courses'
import NotFound from '../components/NotFound';
import SubSubject from '../components/SubSubject'


export default function App() { 

   return (
      <ErrorBoundary>
         <Routes>
            <Route path="/signup" element={<Signup />} />
            <Route path="/login" element={<Login />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/home" element={<Courses />} />
            <Route path="/" element={<Courses />} /> 
            <Route path="/add_subject" element={<AddSubjectForm />} />
            <Route path="/add_topic/:id" element={<AddTopicForm />} />
            <Route path="/add-sub-subject/:id" element={<AddSubSubjectForm />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/detail/:id" element={<Detail />} />
            <Route path="/sub-subject/:id" element={<SubSubject />} />
            <Route path="*" element={<NotFound />} />     
         </Routes>
      </ErrorBoundary> 
   )
}

// BrowserRouter is located in store.js



