import { configureStore } from '@reduxjs/toolkit';

import signupReducer from '../features/signup/signupSlice';
import loginReducer from '../features/login/loginSlice';
import logoutReducer from '../features/logout/logoutSlice';
import contentReducer from '../features/contents/contentsSlice';
import { useDispatch } from 'react-redux';


const store = configureStore({
   reducer: {
      userSignup: signupReducer,
      userLogin: loginReducer,
      userLogout: logoutReducer,
      edusContent: contentReducer
   },
})

export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch 

export type RootState = ReturnType<typeof store.getState>
export default store;