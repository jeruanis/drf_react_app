import { useParams, useLocation } from 'react-router-dom';
import React, { useEffect, useState, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { selectSubSubjectData, selectHomeEdusData, getSubSubject, removeSubSubjectNewItemFlag, deleteSubSubject } from '../features/contents/contentsSlice'
import { useGetTokenCookie } from '../utilities/cookieAccess';
import { useGetSubjectName, useCapitalize, useLimitCharacters} from '../utilities/customHooks'
import { Tooltip } from './Tooltip' 


import  Spinner from './Spinner'
import Button from './Button'
import { IconDelete, IconPlus } from './Icons'
import NavComponent from './NavBar' 
import ListItems from './ListItems'
import { useAppDispatch } from '../app/store';
import { SubSubject } from '../features/contents/contentTypes'


const SubSubjectComponent = () => {
        const [showComponent, setShowComponent] = useState<boolean>()
        const dispatch = useAppDispatch()
        const token = useGetTokenCookie('token')
        const capitalize = useCapitalize()
        const limitCharacters = useLimitCharacters()
        const { id } = useParams();
        const subjectId = id ? parseInt(id) : '';
        const location = useLocation()
        const subjectNameFromCat = location.state?.subjectNamefromCat;
        const [ subjectName, setSubjectName ] = useState()
        const selectSsData = useSelector(selectSubSubjectData)
        const selectHomeContent = useSelector(selectHomeEdusData)
        const getSubjectName = useGetSubjectName()
        const [isEditMode, setIsEditMode] = useState<boolean>(false)

        const filteredData = useMemo(()=>{
           return Object.values(selectSsData.edusData)
                 .filter(value => value.subject === subjectId)
        },[subjectId, selectSsData.edusData]);

        const highestChosenTopic = filteredData.reduce((prev, curr) => {
           return (prev.id > curr.id) ? prev : curr;
         }, { id: -Infinity });

        const highestChosenTopicId = highestChosenTopic.id;
        const choosenCourse = filteredData.find(item=>item.subject === (subjectId || highestChosenTopicId));
        const [ selectedItem, setSelectedItem ] = useState<SubSubject | null>()
        const [ slugArray, setSlugArray ] = useState<SubSubject[]>([])


        const handleDelete = () => {
           if(selectedItem){
              const subSubjectId = selectedItem.id
              try{
                 dispatch(deleteSubSubject({subSubjectId, token}))
                 setSelectedItem(null)
              }catch(e){
                 console.error("Error deleting: ", e)
              }
           }
        }

        const handleEditClick = (e: React.MouseEvent<HTMLButtonElement>) => {
           setIsEditMode(true);
         };


        useEffect(()=>{
           const timer = setTimeout(()=>{
              setShowComponent(true)
           }, 300)

           return ()=> clearTimeout(timer)
        }, [])


        useEffect(() => {
           let shouldFetchData = false;
              if (selectSsData.newItem === true) {
                 dispatch(getSubSubject({token, subjectId}));
                 dispatch(removeSubSubjectNewItemFlag());
              } else if (selectSsData && Object.keys(selectSsData).length > 0) {
                 if (filteredData.length === 0) {
                    shouldFetchData = true;
                 } else {
                    setSlugArray(filteredData);
                 }

                 if(choosenCourse){
                    if(selectedItem){
                       if(selectedItem.subject === subjectId){
                          setSelectedItem(prev=>prev)
                       }else{
                          setSelectedItem(choosenCourse)
                       }
                    }else{
                       setSelectedItem(choosenCourse)
                    }
                 }else {
                    setSelectedItem(null)
                 }

              } else {
                 shouldFetchData = true;
              }

              if (shouldFetchData) {
                 dispatch(getSubSubject({token, subjectId}));
              }

              if(highestChosenTopicId === -Infinity){
                 setSlugArray([])
              }

        }, [highestChosenTopicId, selectedItem, dispatch, selectSsData.newItem,choosenCourse,filteredData,selectSsData,subjectId,token]); // #vup removed selectDetailContent this make the re-render infinite.


        useEffect(()=>{
           setSubjectName(getSubjectName(selectHomeContent.homeEdusData, id))
        },[subjectNameFromCat, getSubjectName,id,selectHomeContent.homeEdusData])


          return (
                 <>
                 <NavComponent />
                 <div className="w-full flex flex-wrap mx-auto px-2 my-10 pt-40 md:pt-16 lg:fixed h-full">
                    { showComponent ? (
                    <>
                       <div className={`w-full md:w-2/6 lg:pl-2 text-xl text-gray-800 leading-normal`}>

                          {/* left side header  */}
                          <header className="z-10">
                             <p className="text-2xl mb-4 font-black">Subject: {subjectName || subjectNameFromCat}</p>

                             <div className="flex justify-between">
                                <p className="text-base font-bold pt-2 lg:pb-6 text-gray-700 lg:mb-2">Topics</p>
                                <Button addclass="" config={{path: `/add-sub-subject/${id}`}} >
                                   <Tooltip content="Add Topic">
                                      <IconPlus />
                                   </Tooltip>
                                </Button>
                             </div>
                          </header>
                          <hr className="border-b border-gray-300 mb-4" />

                          <div className="w-full inset-0 overflow-x-hidden overflow-y-auto lg:overflow-y-hidden lg:block mt-0 bg-white shadow-lg shadow-indigo-500/40 lg:bg-transparent pb-2" id="menu-content">
                             <ul className="list-reset lg:overflow-auto lg:h-65">

                             {(selectedItem || selectSsData.newItem || slugArray || highestChosenTopicId) ?  (Object.values(slugArray)
                                .sort((a, b) => a.id-b.id)
                                .map((dict, index) => (
                                <li key={index} className="md:my-0 hover:bg-purple-100 lg:hover:bg-transparent cursor-pointer"
                                onMouseDown={()=>setSelectedItem(dict)}>
                                   <span className={`${dict?.slug === selectedItem?.slug ? "bg-purple-500 text-white" : "text-lime-700" } block pl-4 pr-6 align-middle no-underline border-l-4 border-transparent lg:hover:border-gray-400 font-serif mb-5 md:pb-0 text-lg font-bold hover:bg-purple-500 hover:text-white break-all`}>

                                      {limitCharacters(capitalize("* "+ dict.name ), 30)}
                                   </span>
                                   </li>
                                   )
                                 )
                                 ): (
                                   <span className={` block pl-4 align-middle text-gray-900 no-underline border-l-4 border-transparent lg:hover:border-gray-400 font-serif mb-5 md:pb-0 text-lg font-bold hover:bg-purple-500 hover:text-white`}>
                                      No Topic available
                                   </span>
                                 )}
                             </ul>
                          </div>
                       </div>

                       {/* right side detail */}

                       { isEditMode ?  (
                          <div className={`h-full w-full md:w-4/6 my-6 lg:mt-0 text-gray-900 leading-normal bg-white border border-gray-400 border-rounded`}>
                             <form className="relative w-full h-full" onSubmit={(e) => {
                                e.preventDefault(); }}>
                             </form>
                          </div>

                          ) : (

                       <div style={{'overflow':'auto'}} className={`w-full md:w-4/6 px-3 mt-6 mb-24 pb-24 lg:mt-0 text-gray-900 leading-normal bg-white overflow-y-scroll h-screen-90`}>
                          <div className="font-sans">
                             <div className="flex justify-between pb-3">
                                <div>
                                   <h1 className="font-medium font-sans break-normal text-gray-900 text-xl inline-block">{ selectedItem && selectedItem.name }
                                   </h1>
                                   <button className="inline-block align-top" onClick={handleDelete}>
                                      {selectedItem ? (
                                      <Tooltip content="Delete Topic">
                                         <IconDelete />
                                         </Tooltip>
                                      ) : null }
                                   </button>
                                </div>
                                {selectedItem && (
                                   <div className="mr-6 pr-6">
                                   <button onClick={handleEditClick} >
                                   </button>
                                   </div>
                                )}

                             </div>
                             <hr className="border-b border-gray-300 mb-4" />
                          </div>

                          <div className="font-sans">
                             <div className="flex justify-between pb-3">
                                <h1 className="text-2xl font-semibold">List of Topics</h1>
                             </div>
                             <hr className="border-b border-gray-300 mb-4" />
                          </div>

                          <section className="py-6 text-lg">
                             {selectedItem && token && typeof token === 'string' && (
                                <ListItems
                                subSubjectId={selectedItem.id}
                                subjectId={subjectId}
                                token={token}
                                />
                             )}
                          </section>
                       </div>
                          )}
                    </>

                    ) : (

                      <Spinner />

                    ) }

                 </div>
              </>
          )
}

export default SubSubjectComponent;
