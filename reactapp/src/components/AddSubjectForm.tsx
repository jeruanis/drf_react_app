

import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useGetTokenCookie } from '../utilities/cookieAccess';
import { homeAddContent, selectHomeEdusData, homeEditContent, hasErrorHome} from '../features/contents/contentsSlice'
import { useNavigate, useParams, useLocation } from 'react-router-dom'
import BackLink from './BackLink' 
import ErrorDisplay from './ErrorDisplay'
import NavComponent from './NavBar'
import { useAppDispatch } from '../app/store';

const AddSubjectForm = () => {
  const nameRef = useRef<HTMLInputElement | null>(null)
  const descriptionRef = useRef<HTMLTextAreaElement | null>(null)
  const dispatch = useAppDispatch();
  const navigate = useNavigate()
  const location = useLocation()
  const queryParams = new URLSearchParams(location.search) 
  const pointParam = queryParams.get('p')
  
  const [edit, setEdit] = useState<boolean>(false)
  const [subjectId, setSubjectId] = useState<number | undefined>()
  const [subject_name, setSubjectName] = useState<string>('')
  const [subjectFocus, setSubjectFocus] = useState<boolean | undefined>()
  const [ error, setError ] = useState() 

  const [description, setDescription] = useState<string>('')
  const [descriptionFocus, setDescriptionFocus] = useState<boolean | undefined>()
  const [title, setTitle] = useState<string | undefined>()

  const token = useGetTokenCookie('token')
  const addedContent = useSelector(selectHomeEdusData)

  const homeError = useSelector(hasErrorHome)
  
  useEffect(()=>{
    if(pointParam){
      const pList = pointParam.split('-')
      setSubjectId(parseInt(pList[pList.length-1]))
      if(pList[0]==='edit'){
        setEdit(true)
      }  

    }
  },[pointParam, edit, subjectId]) 

  useEffect(()=>{
    if(!homeError){
      if(addedContent?.newItem === true){ 
        navigate('/courses')
        return 
      }
    }
  },[addedContent, homeError, navigate]) 

  useEffect(()=>{
    if(edit){
      if(addedContent?.homeEdusData){
        for (const [key, value] of Object.entries(addedContent.homeEdusData)){
          if(value.id === subjectId){
            setSubjectName(value.subject_name)
            setDescription(value.description) 
            setSubjectId(value.id)
            if(nameRef.current){
              nameRef.current.value = subject_name;
            }
            if(descriptionRef.current){
              descriptionRef.current.value = description; 
            }
            
          }
        } 
      }
      
    } 
  }, [pointParam, addedContent, edit, nameRef, descriptionRef])   

  useEffect(()=>{ 
    if(edit && subject_name)
      setTitle(`Edit ${subject_name}`)
    else
      setTitle('Adding new subject')   
  }, [edit, subject_name])
  

  //flag the store that there is a new one when successfully added. using newItem.
  const handleSubmit = async (e:React.FormEvent<HTMLFormElement>) =>{ 
    e.preventDefault()
    if(edit){
      dispatch(homeEditContent({subjectId, subject_name, description, token}))
    }else
      dispatch(homeAddContent({subject_name, description, token})) 
  }


  return (
    <>
    <NavComponent />
    <div className="mt-24 mb-1 container px-4"> 
      <BackLink />
      <p className="text-2xl font-bold mb-6">{title}</p>

      <form action="" method="POST" className="w-full max-w-xl" onSubmit={handleSubmit}>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
              Subject
            </label>
          </div>
          <div className="md:w-2/3">
            <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id="subject" type="text" 
            placeholder="Subject name"
            autoComplete="off"
            ref={nameRef}
            value={subject_name}
            onChange={(e)=>setSubjectName(e.target.value)}
            onFocus={()=>setSubjectFocus(true)}
            onBlur={()=>setSubjectFocus(false)} 
             />
          </div>
        </div>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-password">
              Description
            </label>
          </div>
          <div className="md:w-2/3">
            <textarea className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Content description here.."
            id="description"
            ref={descriptionRef}
            value={description}
            autoComplete="off"
            onChange={(e)=>setDescription(e.target.value)}
            onFocus={()=>setDescriptionFocus(true)}
            onBlur={()=>setDescriptionFocus(false)}
            ></textarea>
          </div>
        </div>

        <div className="md:flex md:items-center">
          <div className="md:w-1/3"></div>
          <div className="md:w-2/3">
            <button className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
              Add
            </button>
          </div>
        </div>
      </form>
    </div>
    <ErrorDisplay errorObj={homeError}/>
    </> 
  )
}

export default AddSubjectForm;
