import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Footer from './Footer'
import { useDispatch, useSelector } from 'react-redux'
import { homeGetContents, selectHomeEdusData, removeHomeNewItemFlag, detailGetContent } from '../features/contents/contentsSlice'
import { useGetTokenCookie } from '../utilities/cookieAccess';
import Button from './Button'
import { IconPlus, IconDelete } from './Icons'
import DropDown from './Dropdown'
import { loginHasError } from '../features/login/loginSlice'
import Spinner from './Spinner'
import { useLimitCharacters } from '../utilities/customHooks'
import NavComponent from './NavBar'
import { useAppDispatch } from '../app/store';

let token: string | null; 

const Courses = () => {
   const dispatch = useAppDispatch();
   token = useGetTokenCookie('token')
   const selectSubjects = useSelector(selectHomeEdusData);
   const homeNewItem = selectSubjects?.newItem ?? undefined;
   const navigate = useNavigate()
   const [showComponent, setShowComponent] = useState(false)
   const limitCharacters = useLimitCharacters()
   const errorObj = useSelector(loginHasError)

   useEffect(() => {
      document.body.style.background = '#ffffff';
   },[])

   useEffect(()=>{
      if(errorObj){
         navigate('/login?q=login-required')
      }
   })
  

   useEffect(()=>{
      const timer = setTimeout(()=>{
         setShowComponent(true)
      }, 300)
      return () => clearTimeout(timer);  
   }, [])


   useEffect(()=> {
      if(selectSubjects?.newItem === true){  
         if(token !== null)
            dispatch(homeGetContents(token))
         dispatch(removeHomeNewItemFlag())

      }else if(selectSubjects?.homeEdusData[0]?.subject_name){
         // skip
      }else if(token){
         dispatch(homeGetContents(token))
      
      }else 
         navigate('/login?q=login-required')
         return
   },[dispatch, homeNewItem, navigate, selectSubjects?.homeEdusData, selectSubjects?.newItem, token]) 

   return (
      <>
      <NavComponent />
      <div className="flex flex-col h-screen w-full mx-auto mt-10 lg:mt-2">
      <div className="flex-grow">

      <div className="mt-24 mb-1 px-4 w-full pt-16 md:pt-2">
         { showComponent ? (
           <>
            <header className="flex justify-between">
               <div className="flex items-center">
                  <IconPlus />
                  <span className="py-1 font-bold text-xl ml-2">Add Subject</span>
               </div>
               <Button addclass="text-white font-bold bg-purple-500 hover:bg-purple-700" config={{path: "/add_subject/?p=add"}} >
                  + add subject
               </Button>
            </header>
            <hr className="border-b border-gray-300 mb-4" />

            <main className="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 mx-auto">
            {selectSubjects?.homeEdusData && Object.values(selectSubjects.homeEdusData).map((dict, index)=>{
                  return ( 
                     <div key={index} className="relative">
                        <div className="font-serif border-solid border border-gray-300 rounded-2xl mr-3 mb-3 shadow-lg">
                           <Link to={`/sub-subject/${dict.id}`} className="text-body">
                              <div className="py-2 px-3">
                                    <h6 className="font-semibold mb-2 text-lg">{dict.subject_name}</h6>
                                    <p className="text-gray-700 text-base">
                                    {limitCharacters(dict.description, 36) || "..."} 
                                 </p>
                              </div>
                           </Link>
                        </div>    
                        <DropDown token={token} subjectId={dict.id}/>   
                     </div>
                  )
               })} 
            </main> 
             </>

            ) : (

               <Spinner />

            )
        } 
      
      </div>
      </div>
      <Footer />
      </div>
      </>

   )
} 
export default Courses;


