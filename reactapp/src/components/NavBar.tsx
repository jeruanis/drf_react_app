import React, { useEffect, useState, useRef, useCallback } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { selectCurrentUser } from '../features/signup/signupSlice'
import { selectCurrentUserLogin, 
         userKeepLogin,
         clearToHome } from '../features/login/loginSlice';
import { useGetTokenCookie } from '../utilities/cookieAccess';
import cicon from '../assets/img/newlogonobg2.png'
import profpic1 from '../assets/img/1.jpg'
import profpic2 from '../assets/img/2.jpg'
import CategoryDropdown from './CategoryDropdown'
import SearchBar from './SearchBar'
import { useAppDispatch } from '../app/store';

const NavComponent = () => {

  const navLinks = [{'name': 'Home', 'path':'/home'}, {'name': 'Courses', 'path': '/courses'}, {'name': 'About', 'path': '/about'}]

  const [username, setUsername] = useState<string>(); 
  const [imageUrl, setImageUrl] = useState<string>();
  const [ activeLink, setActiveLink ] = useState(0);
  const [menuVisible, setMenuVisible] = useState<boolean>();
  const [profileMenuVisible, setProfileMenuVisible] = useState(false);
  const mainMenuRef = useRef<HTMLButtonElement>(null);
  const profileMenuRef = useRef<HTMLButtonElement>(null);
  const mobileMenuVisiblePadRef = useRef();


  const location = useLocation(); 
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const isSignupPage = location.pathname === '/signup';
  const isHomePage = location.pathname === '/home';
  const isHomePageDefault = location.pathname === '/';
  const pathnameArr = location.pathname.split('/')


  const queryParams = new URLSearchParams(location.search);
  const logoutParams = queryParams.get('logout');

  const tokenCookie = useGetTokenCookie('token');

  const signupUserData = useSelector(selectCurrentUser);
  const loginUserData = useSelector(selectCurrentUserLogin)
  

  useEffect(() => { 

    const toggleDoc = (event: any) => {
      event.stopPropagation();
      if (mainMenuRef.current && mainMenuRef.current.contains(event.target)) {
        setMenuVisible(prevState => !prevState);
      } else if (profileMenuRef.current && profileMenuRef.current.contains(event.target)) {
        setProfileMenuVisible(prevState => !prevState);
      }
    }
  

    const docToggle = () => {
      setMenuVisible(false);
      setProfileMenuVisible(false);
    }

    if (mainMenuRef.current)
      mainMenuRef.current.addEventListener('click', toggleDoc);
    
    if (profileMenuRef.current)
      profileMenuRef.current.addEventListener('click', toggleDoc);
    
    document.addEventListener('click', docToggle);
  
    return () => {
      document.removeEventListener('click', docToggle);
  
      if (mainMenuRef.current)
        mainMenuRef.current.removeEventListener('click', toggleDoc);
      
      if (profileMenuRef.current)
        profileMenuRef.current.removeEventListener('click', toggleDoc);
    }
  
  }, [profileMenuRef?.current, mainMenuRef?.current, navigate, location.pathname, menuVisible, profileMenuVisible]);


  useEffect(()=>{
     if(signupUserData){
      if(signupUserData?.username && signupUserData?.imageUrl) {
          setUsername(signupUserData.username);
          if(signupUserData.imageUrl === '1.jpg'){
              setImageUrl(profpic1)
          } else if(signupUserData.imageUrl === '2.jpg'){
              setImageUrl(profpic2)
          } else {  
              setImageUrl(signupUserData.imageUrl)
          } 
      }
    }

  }, [signupUserData?.username, signupUserData?.imageUrl, imageUrl])

  
  useEffect(()=>{
    if (loginUserData) {
        if(loginUserData?.tohome){
          dispatch(clearToHome())
        }

        if(loginUserData?.username && loginUserData?.imageUrl && loginUserData?.tohome){
            setUsername(loginUserData.username);
            if(loginUserData.imageUrl === '1.jpg')
                setImageUrl(profpic1)
            else if(loginUserData.imageUrl === '2.jpg')
                setImageUrl(profpic2)
            else 
            setImageUrl(loginUserData.imageUrl)
        }
    }


  },[navigate, isHomePage, isHomePageDefault, isSignupPage, dispatch, loginUserData?.username, imageUrl])  
  

  useEffect(()=>{
    if(logoutParams == 'success'){
      return;
    } else if (signupUserData?.username && signupUserData?.imageUrl) {
       setUsername(signupUserData.username);
    } else { 
      if(tokenCookie){
        dispatch(userKeepLogin(tokenCookie)) 
      }
    }
  }, [])

   return (

      <header className="shadow-lg shadow-zinc-200 w-full fixed top-0 left-0 z-20">
        <nav className="bg-white">
          <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
            <div className="relative flex h-20 items-center justify-between"> 
            {/* wrapper group end */}

              {/* <!-- Mobile menu button--> */}
              <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                <button ref={mainMenuRef} type="button" className="relative inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
                  <span className="absolute -inset-0.5"></span> 
                  <span className="sr-only">Open main menu</span>

                  <svg className="block h-6 w-6" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                  </svg>

                  <svg className="hidden h-6 w-6" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                  </svg>
                </button>
              </div>

              {/* Nav Links */}
              <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                <div className="flex flex-shrink-0 items-center">
                 <Link to={'/home'}>
                    <img className="h-8 w-auto" src={cicon} alt="logo" />
                  </Link>
                </div>
                <div className="hidden md:ml-6 sm:block">
                  <div className="flex space-x-4">

                    {navLinks.map((link, index) => {  
                        const isActive = location.pathname === link.path;       
                        return (
                            <Link 
                                key={index} 
                                to={link.path} 
                                className={`text-gray-600 rounded-md px-3 py-2 text-sm font-medium 
                                ${isActive ? "bg-gray-900 text-white" : ''} hover:bg-gray-900 hover:text-white`}  
                                aria-current={isActive ? "page" : undefined}
                            >
                                {link.name}
                            </Link>
                        )
                    })}

                    { //show Category in detail page only
                      (pathnameArr[1] === 'sub-subject' || pathnameArr[1] === 'detail') ?  
                        <CategoryDropdown />
                      : null
                    } 

                    {/* Search input large screen*/}
                    <span className="sm:hidden md:hidden lg:inline-block">
                      <SearchBar config={{mainMenuRef:mainMenuRef, profileMenuRef:profileMenuRef}}/>
                    </span>

                  </div>
                </div>
              </div>

              {/* Right side content of the navbar */}
              {username ? ( 
              <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                <button type="button" className="relative rounded-full bg-gray-800 p-1 text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800">
                  <span className="absolute -inset-1.5"></span>
                  <span className="sr-only">View notifications</span>
                  <svg className="h-6 w-6" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" aria-hidden="true">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0" />
                  </svg>
                </button>

                {/* <!-- Profile dropdown --> */}
                <div className="relative ml-3">
                  <div>
                    <button type="button" ref={profileMenuRef} className="relative flex rounded-full bg-gray-800 text-sm focus:outline-none focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
                      <span className="absolute -inset-1.5"></span>
                      <span className="sr-only">Open user menu</span>
                      <img className="h-8 w-8 rounded-full" title={username} src={imageUrl && imageUrl}  
                      alt="" />    
                    </button>
                  </div>
                  
                  <div className={`absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-blue ring-opacity-5 hover:outline-gray-100 ${profileMenuVisible ? "": "collapse"}`} role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabIndex={-1}>


                    <Link to={"/profile"} className="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabIndex={-1} id="user-menu-item-0">Profile</Link>
                    <a href="#" className="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabIndex={-1} id="user-menu-item-1">Settings</a>
                    <Link to={"/logout"} onClick={e => e.stopPropagation()} className="block px-4 py-2 text-sm text-gray-700" role="menuitem" tabIndex={-1} id="user-menu-item-2">Sign out</Link>
                  </div>
                </div>
                </div>

                ) : (  

                  <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                    <div className="d-inline-block">
                      <Link to={ "/login" } className="text-gray-400 hover:bg-gray-700 hover:text-white rounded-md px-3 py-2 text-sm font-medium">Login</Link>
                    </div>
                  </div>

                  )}
              
            </div>
 

          </div>

          {/* <!-- Mobile menu, show/hide based on menu state. --> */}
          <div className={`${menuVisible ? "hidden" : "block"} md:hidden pb-3`} id="mobile-menu">
            <div className="space-y-1 px-2 pb-3 pt-2">

                {navLinks.map((link, index) => {  
                    return(
                      <Link key={index} to={ link.path } className={`text-gray-600 rounded-md px-3 py-2 text-sm font-medium 
                      ${activeLink === index ? "bg-gray-900 text-white" : ''} hover:bg-gray-900 hover:text-white`}  
                      onMouseDown={()=>setActiveLink(index)}
                      aria-current={location.pathname === link.path ? "page" : undefined}
                      >{link.name}</Link>
                    )
                })}

            </div>

            {/* Search input mobile show at md since it is hidden in large screen*/}
            <span className="sm:hidden lg:inline-block md:inline-block">
              <SearchBar config={{mainMenuRef:mainMenuRef, profileMenuRef:profileMenuRef}}/>
             </span>
            
          </div>
        </nav>
      </header>

   )
}
export default NavComponent;



