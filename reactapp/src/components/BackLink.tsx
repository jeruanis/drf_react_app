
import React from 'react';
import { useNavigate } from 'react-router-dom'

const BackLink = () => {
  const navigate = useNavigate()

  return (
    <button onClick={()=>navigate(-1)}>
         <span className="text-base text-purple-500 font-bold">&laquo;</span> <a href="#" className="text-base md:text-sm text-purple-500 font-bold no-underline hover:underline">Back Link</a>
    </button> 
  )
}

export default BackLink;