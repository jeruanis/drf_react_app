import React, { Fragment, useEffect } from 'react'
import { Menu, Transition } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/20/solid'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux';
import { deleteSubject, selectHomeEdusData, removeHomeNewItemFlag, homeGetContents } from '../features/contents/contentsSlice'
import { useAppDispatch } from '../app/store';

function classNames(...classes: (string | boolean)[]) {
  return classes.filter(Boolean).join(' ');
}

interface DeleteSubject {
  subjectId?: number;
  token: string | null;
}

export default function DropDown({subjectId, token}:DeleteSubject) {

  const dispatch = useAppDispatch()

  const handleDelete = () => {
    if(subjectId && token){
      dispatch(deleteSubject({subjectId, token}))
    }
  }

  const selectHomeData = useSelector(selectHomeEdusData)

  useEffect(() => {
    const fetchData = async () => {
      if (selectHomeData?.newItem === true) {
          dispatch(removeHomeNewItemFlag());
          if(token){
            dispatch(homeGetContents(token));
          }
      } 
    };

    fetchData();

  }, [dispatch, selectHomeData.newItem]);


  return (
    <Menu as="div" className="absolute top-2 right-2">
      <div>
      <Menu.Button className="inline-flex w-full justify-center gap-x-1.5 rounded-md bg-purple px-3 pb-2 text-sm font-semibold text-gray-900 hover:bg-purple-100">
          <ChevronDownIcon className="-mr-1 h-5 w-5 text-gray-400" aria-hidden="true" />
        </Menu.Button>
      </div>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95" 
      >
         {/* main menu */}
        <Menu.Items className="absolute right-0 z-10 mt-2 w-24 mr-2 origin-top-right bg-gray-100 focus:outline-none">
          <div className="w-auto bg-gray-200">
            {/* submenu: the width of main menu should be the same as submenu */}
            <Menu.Item>
              {({ active }) => (
                <Link to={`/add_subject/?p=edit-${subjectId}`} 
                  className={classNames(
                    active ? 'bg-purple-200 text-gray-900 block text-center px-4 py-2 w-24' : 'text-gray-700 block text-center px-4 py-2 w-24'
                  )}
                >
                  edit
                </Link>
              )}
            </Menu.Item>
            <Menu.Item>
              {({ active }) => (
                <button onClick={handleDelete}
                  className={classNames(
                    active ? 'bg-purple-200 text-gray-900 block text-center px-4 py-2 w-24' : 'text-gray-700 block text-center px-4 py-2 w-24'
                  )}
                >
                  delete
                </button>
              )}
            </Menu.Item>
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  )
}