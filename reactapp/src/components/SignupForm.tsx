import React, { useEffect, useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import ErrorDisplay from './ErrorDisplay'
import { userSignup, signupHasError } from '../features/signup/signupSlice' 
import companyicon from '../assets/img/newlogonobg2.png'
import { useAppDispatch } from '../app/store';
import { UserSignupInputType } from '../features/signup/signupType'


const NAMELASTNAME_REGEX = /^[A-Za-z\s]{2,25}$/;
const EMAIL_REGEX =  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/; 
const PASSWORD_REGEX = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{5,30}$/;
const bodyStyle={
   backgroundColor:'linear-gradient(45deg, #20bf6b, #0088cc, #ebf8e1 70%, #f89406, white)',
   backgroundSize: 'cover',
   backgroundRepeat: 'no-repeat',
} 


const SignupForm = () => {
     const nameRef = useRef<HTMLInputElement | null>(null);
     const lastNameRef = useRef<HTMLInputElement | null>(null);
     const emailRef = useRef<HTMLInputElement | null>(null);
     const matchEmailRef = useRef<HTMLInputElement | null>(null);
     const pwdRef = useRef<HTMLInputElement | null>(null);
     const matchPwdRef = useRef<HTMLInputElement | null>(null);
     const fnamesp=useRef<HTMLInputElement | null>(null)
     const lnamesp=useRef<HTMLInputElement | null>(null)
     const emailsp=useRef<HTMLInputElement | null>(null) 
     const email2sp=useRef<HTMLInputElement | null>(null)
     const passwordsp=useRef<HTMLInputElement | null>(null)
     const password2sp=useRef<HTMLInputElement | null>(null)
     const recaptchaRef = useRef()

     const [name, setName] = useState<string>();
     const [validName, setValidName] = useState<boolean>();
     const [nameFocus, setNameFocus] = useState<boolean>();

     const [lastName, setLastName] = useState<string>();
     const [validLastName, setValidLastName] = useState<boolean>();
     const [lastNameFocus, setLastNameFocus] = useState<boolean>();

     const [email, setEmail] = useState<string>();
     const [validEmail, setValidEmail] = useState<boolean>();
     const [emailFocus, setEmailFocus] = useState<boolean>();
  
     const [confirmEmail, setConfirmEmail] = useState<string>();
     const [validConfirmEmail, setValidConfirmEmail] = useState<boolean>();
     const [confirmEmailFocus, setConfirmEmailFocus] = useState<boolean>();
  
     const [pwd, setPwd] = useState<string>();
     const [validPwd, setValidPwd] = useState<boolean>();
     const [pwdFocus, setPwdFocus] = useState<boolean>();
  
     const [matchPwd, setMatchPwd] = useState<string>();
     const [validMatchPwd, setValidMatchPwd] = useState<boolean>();
     const [matchPwdFocus, setMatchPwdFocus] = useState<boolean>();
  
     const [recaptchaVerified, setRecaptchaVerified] = useState()

     const dispatch = useAppDispatch()

     const errorObj = useSelector(signupHasError)

  useEffect(()=> {
    if(nameRef.current)
       nameRef.current.focus();
  }, [])


  useEffect(() => {
      if(typeof name === 'string'){
         const result = NAMELASTNAME_REGEX.test(name);
         setValidName(result)
         if(fnamesp.current)
            fnamesp.current.textContent = nameFocus && !validName ? 
            'Only letters and space allowed and 2 to 25 characters only' : nameFocus===false && !validName ?  'Only letters and space allowed and 2 to 25 characters only' : '';
      }

  }, [name, validName, nameFocus])


  useEffect(() => {
    if(typeof lastName === 'string'){
      const result = NAMELASTNAME_REGEX.test(lastName);
      setValidLastName(result);
      if(lnamesp.current)
         lnamesp.current.textContent = lastNameFocus && !validLastName ? 'Only letters and space allowed and 2 to 25 characters only' : lastNameFocus===false && !validLastName ? 'Only letters and space allowed and 2 to 25 characters only' : '';
    }

  }, [lastName, validLastName, lastNameFocus])


  useEffect(()=>{
    if(typeof email === 'string'){
      const result = EMAIL_REGEX.test(email)
      setValidEmail(result)
      if(emailsp.current)
         emailsp.current.textContent = emailFocus && !validEmail ? 'Invalid email format' : emailFocus===false && !validEmail ? 'Invalid email format' : email !== confirmEmail ? 'Email mismatched': '';
    }

  }, [email, validEmail, emailFocus, email2sp?.current?.textContent])


  useEffect(()=>{
    if(typeof confirmEmail === 'string'){
      const result = EMAIL_REGEX.test(confirmEmail)
      setValidConfirmEmail(result)

      if(email2sp.current)
         email2sp.current.textContent = confirmEmailFocus === false && (email !== confirmEmail) ? 'Email mismatched' : confirmEmailFocus && (email !== confirmEmail) ? 'Email mismatched' : confirmEmailFocus && !validConfirmEmail ? 'Invalid email format' : confirmEmailFocus===false && !validConfirmEmail ? 'Invalid email format' : '';
    }
    
  },[confirmEmail, validConfirmEmail, email, confirmEmailFocus, emailsp?.current?.textContent])


  useEffect(()=>{
    if(email !== confirmEmail)
       setValidConfirmEmail(false);
  },[email, confirmEmail, validConfirmEmail])


  useEffect(()=>{
    if(typeof pwd === 'string'){
      const result = PASSWORD_REGEX.test(pwd)
      setValidPwd(result)
      if(passwordsp.current)
         passwordsp.current.textContent = pwdFocus && !validPwd ? 
         'Password should be 5 to 30 characters with at least one letter, one number and one special character' : pwdFocus===false && !validPwd ? 'Password should be 5 to 30 characters with at least one letter, one number and one special character' : '';
    }
  }, [pwd, validPwd, pwdFocus])

 
  useEffect(()=>{
    if(typeof matchPwd === 'string'){
    const result = PASSWORD_REGEX.test(matchPwd)
    setValidMatchPwd(result)
    if(password2sp.current)
       password2sp.current.textContent = matchPwdFocus && !validMatchPwd ? 
        'Password should be 5 to 30 characters with at least one letter, one number and one special character' : matchPwdFocus===false && !validMatchPwd ? 'Password should be 5 to 30 characters with at least one letter, one number and one special character' : matchPwdFocus && pwd !== matchPwd ? 'Password mismatched' : matchPwdFocus===false && pwd !== matchPwd ? 'Password mismatched' : '';
    }
  }, [matchPwd, validMatchPwd, pwd, matchPwdFocus])


  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      dispatch(userSignup({name, lastName, email, confirmEmail, pwd, matchPwd}))
  }


  return (
       <main className="flex items-center justify-center h-screen bg-gradient-to-r from-white to-sky-400">
        <div className="flex mx-auto w-1/3justify-center md:justify-between rounded-md item-center bg-green-500">   
               <div className="w-50 bg-white rounded-lg">
                <div className="p-6 font-normal">
                     {errorObj ? (
                        <div className="pb-1 mb-3 bg-red-300 rounded"> 
                        <ErrorDisplay errorObj={errorObj} />  
                        </div>
                        ) : ( 
                        <div className="flex justify-center mb-3">
                           <Link to={"/home"}><img src= {companyicon} alt="Invoice logo"/></Link>
                        </div> 
                     )}
                  <form onSubmit={handleSubmit} id="regForm">

                     <label htmlFor="name" style={{width:'110px'}}> First Name : </label>
                     <input className="border-b-2 mb-3 w-60 ml-2 p-2 rounded-lg focus:outline-none"
                        type="text" 
                        id="name" 
                        ref={nameRef} 
                        autoComplete="off"
                        name="fist name"
                        spellCheck="false" 
                        onChange={(e)=>setName(e.target.value)} 
                        aria-invalid={validName ? "false" : "true"} 
                        aria-describedby="fname"
                        onFocus={()=>setNameFocus(true)} 
                        onBlur={()=>setNameFocus(false)} 
                        required />
                        <p className="text-red-600 max-w-sm" id="fname" ref={fnamesp}></p>
                                 
                     <label htmlFor="lastName" style={{width:'110px'}}>Last Name :</label>  
                     <input className="border-b-2 mb-3 w-60 ml-2 p-2 rounded-lg focus:outline-none"
                        type="text" 
                        id="lastName"
                        ref={lastNameRef}
                        name="last namd"
                        autoComplete="off"
                        spellCheck="false" 
                        onChange={(e)=>setLastName(e.target.value)} 
                        aria-invalid={validLastName ? "false" : "true"} 
                        aria-describedby="lname" 
                        onFocus={()=>setLastNameFocus(true)} 
                        onBlur={()=>setLastNameFocus(false)}  
                        required />
                        <p className="text-red-600 max-w-sm" id="lname" ref={lnamesp}></p>
                     
                     <label htmlFor="email" style={{width:'110px'}}>Email :</label>
                     <input className="border-b-2 mb-3 w-64 ml-2 p-2 rounded-lg focus:outline-none"
                        type="email" 
                        id="email" 
                        ref={emailRef}
                        autoComplete="off"
                        name="new email"
                        spellCheck="false" 
                        onChange={(e)=>setEmail(e.target.value)} 
                        aria-invalid={validEmail ? "false" : "true"} 
                        aria-describedby="emailsp" 
                        onFocus={()=>setEmailFocus(true)} 
                        onBlur={()=>setEmailFocus(false)}  
                        required />
                        <p className="text-red-600 max-w-sm" id="emailsp" ref={emailsp}></p>
                     
                     <label htmlFor="email2" style={{width:'110px'}}>Confirm Email :</label>
                     <input className="border-b-2 mb-3 w-52 ml-2 p-2 rounded-lg focus:outline-none"
                        type="email" 
                        id="email2" 
                        ref={matchEmailRef}
                        autoComplete="new signup"
                        name="new signup"
                        spellCheck="false" 
                        onChange={(e)=>setConfirmEmail(e.target.value)} 
                        aria-invalid={validConfirmEmail ? "false" : "true"} 
                        aria-describedby="email2sp"
                        onFocus={()=>setConfirmEmailFocus(true)} 
                        onBlur={()=>setConfirmEmailFocus(false)}  
                        required />
                        <p className="text-red-600 max-w-sm" id="email2sp" ref={email2sp}></p>
                        
                     <label htmlFor="password" style={{width:'110px'}}>Password :</label>
                     <input className="border-b-2 mb-3 w-56 ml-2 p-2 rounded-lg focus:outline-none"
                        type="password" 
                        id="password" 
                        ref={pwdRef}
                        autoComplete="off"
                        name="password"
                        onChange={(e)=>setPwd(e.target.value)} 
                        aria-invalid={validPwd ? "false" : "true"} 
                        aria-describedby="pwd" 
                        onFocus={()=>setPwdFocus(true)} 
                        onBlur={()=>setPwdFocus(false)} 
                        required/>
                        <p className="text-red-600 max-w-sm" id="pwd" ref={passwordsp}></p>
                        
                     <label htmlFor="password2" style={{width:'100%'}}>Confirm Password :</label>
                     <input className="border-b-2 mb-3 w-42 ml-2 p-2 rounded-lg focus:outline-none"
                        type="password" 
                        id="password2"
                        autoComplete="off"
                        name="password2"
                        ref={matchPwdRef} 
                        onChange={(e)=>setMatchPwd(e.target.value)} 
                        aria-invalid={validMatchPwd ? "false" : "true"} 
                        aria-describedby="pwd2"
                        onFocus={()=>setMatchPwdFocus(true)} 
                        onBlur={()=>setMatchPwdFocus(false)} 
                        required />
                        <p className="text-red-600 max-w-sm" id="pwd2" ref={password2sp}></p>

                        <div style={{border: "none"}}  className="text-center"> 
                           <span>By registering you agree to the &nbsp;</span>
                           <a target="_blank" rel="noreferrer" href="https://jba.homes/termsandconditions.html" className="block" style={{textAlign: "center", position: "relative", border:"none", color:"#3742fa"}}>Terms and Conditions</a> and <a target="_blank" rel="noreferrer" href="https://jba.homes/privacypolicy.html">Privacy Policy</a>
                        </div>
                        <br />

                     
                        <div className="flex justify-center">
                           <button className={`w-3/6 inline-block text-white py-1 px-4 rounded mb-3 ${validName && validLastName && validEmail && validConfirmEmail &&  validPwd && validMatchPwd  ? "bg-yellow-600" : "bg-yellow-300"}`} disabled={!validName || !validLastName || !validEmail || !validConfirmEmail || !validPwd || !validMatchPwd}>Register</button>
                        </div>

                     <span className="text-center block">
                        <Link to={"/login"} id="signin" className="text-blue-700 px-3 signin">
                           Already have an account? Log in</Link>
                     </span>
                     
                  </form>
               </div>
               </div>
            </div>
         </main>
                        
  )

}

export default SignupForm;