

import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useGetTokenCookie } from '../utilities/cookieAccess';
import { detailAddTopic, selectDetailEdusData, selectHomeEdusData, hasErrorDetail, selectSubSubjectData, selectDetailData } from '../features/contents/contentsSlice'
import { useNavigate, useParams, useLocation } from 'react-router-dom'
import { useGetSubjectName, useGetSubSubjectName } from '../utilities/customHooks';
import BackLink from './BackLink' 
import ErrorDisplay from './ErrorDisplay'
import NavComponent from './NavBar'
import { useAppDispatch } from '../app/store';
import { RootState } from '../app/store';

type addTopicFormState = RootState['edusContent']

const AddTopicForm = () => {
  const { id } = useParams();
  const sub_subject = id;
  const [topic_name, setTopicName] = useState<string | undefined>()
  const [topicFocus, setTopicFocus] = useState<boolean | undefined>()
  const [article, setArticle] = useState<string | undefined>()
  const [articleFocus, setArticleFocus] = useState<boolean | undefined>()
  const [subSubjectName, setSubSubjectName] = useState<string | undefined>()
  const token = useGetTokenCookie('token')
  const selectDetailTopic = useSelector(selectDetailData) 
  const selectSsData = useSelector(selectSubSubjectData)
  const errorDetail = useSelector(hasErrorDetail)
  const dispatch = useAppDispatch();
  const navigate = useNavigate()
  const getSubSubjectName = useGetSubSubjectName()


useEffect(()=>{  
  setSubSubjectName(getSubSubjectName(selectSsData.edusData, id))
}, [subSubjectName, topic_name])

console.log('subSubjectName: ', subSubjectName)

  useEffect(()=>{
    if(selectDetailTopic?.newItem === true){    
      navigate(`/detail/${sub_subject}`)
    }
  },[selectDetailTopic])

 
  const handleSubmit = async (e:React.FormEvent<HTMLFormElement>) =>{ 
    e.preventDefault()
      dispatch(detailAddTopic({topic_name, article, sub_subject, token}))
    } 

  return (
    <>
    <NavComponent />
    <div className="mt-24 mb-1 container px-4"> 

    <BackLink />

    <p className="text-2xl font-bold mb-6">Adding topic to {subSubjectName} </p>

      <form action="" method="POST" className="w-full max-w-xl" onSubmit={handleSubmit}>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
              Topic
            </label>
          </div>
          <div className="md:w-2/3">
            <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id="subject" type="text" 
            placeholder="Add topic"
            autoComplete='off'
            onChange={(e)=>setTopicName(e.target.value)}
            onFocus={()=>setTopicFocus(true)}
            onBlur={()=>setTopicFocus(false)} 
             />
          </div>
        </div>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-password">
              Article
            </label>
          </div>
          <div className="md:w-2/3">
            <textarea className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" placeholder="Content description here.."
            id="description"
            autoComplete="off"
            onChange={(e)=>setArticle(e.target.value)}
            onFocus={()=>setArticleFocus(true)}
            onBlur={()=>setArticleFocus(false)}
            ></textarea>
          </div>
        </div>
  
        <div className="md:flex md:items-center">
          <div className="md:w-1/3"></div>
          <div className="md:w-2/3">
            <button className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
              Add
            </button>
          </div>
        </div>
      </form>
    </div>
    <ErrorDisplay errorObj={errorDetail}/>
    </> 
  )
}

export default AddTopicForm;