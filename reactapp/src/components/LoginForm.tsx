import React, { useEffect, useState, useRef } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import ReCAPTCHA from "react-google-recaptcha";
import { userLogin, loginHasError } from '../features/login/loginSlice'
import { useDispatch, useSelector } from 'react-redux'
import GoogleOauth from './auth/Google'
import ErrorDisplay from './ErrorDisplay'
import { selectCurrentUser } from '../features/signup/signupSlice'
import companyicon from '../assets/img/newlogonobg2.png'
import { useAppDispatch } from '../app/store';
import { UserLoginInputType } from '../features/login/loginType'


const PASSWORD_REGEX = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{5,30}$/;
const LoginForm = () => {
    const emailRef = useRef<HTMLInputElement | null>(null) 
    const passwordRef = useRef<HTMLInputElement | null>(null)   
    const errRef = useRef('') 
    const recaptchaRef = useRef()
    const passwordsp=useRef<HTMLInputElement | null>(null)
    const [ emailFocus, setEmailFocus ] = useState<boolean>();
    const [ email, setEmail ] = useState<string>('');
    const [ validEmail , setValidEmail ] = useState<boolean>();
    const [ passwordFocus, setPasswordFocus ] = useState<boolean>();
    const [ password, setPassword ] = useState<string>();
    const [ validPassword, setValidPassword ] = useState<boolean>();
    const [errMsg, setErrMsg] = useState('')
    const [recaptchaVerified, setRecaptchaVerified] = useState<boolean>();
    const dispatch = useAppDispatch()
    const navigate = useNavigate()
    const signupUserData = useSelector(selectCurrentUser);
    const errorObj = useSelector(loginHasError)

    const onChange = () => {
      setRecaptchaVerified(true);
    }
    
    useEffect(() => {
      const timer = setTimeout(() => { 
        setRecaptchaVerified(false);
      }, 60000);
  
      return () => {
        clearTimeout(timer);
      };
    }, []); 

    useEffect(()=>{  
      if( signupUserData && signupUserData?.email){
        setEmail(signupUserData.email);  
      } 

      if(emailRef && emailRef.current && emailRef.current.value) {
        setEmail(emailRef.current.value)
        setValidEmail(true)
      }else{
          setValidEmail(false)
      }

    },[email, navigate])


    useEffect(()=>{
      if (typeof password === 'string') { 
        const result = PASSWORD_REGEX.test(password);
      
        setValidPassword(result)
        if(passwordsp.current)
          passwordsp.current.textContent = passwordFocus && !validPassword ? 
              'Password should be 5 to 30 characters with at least one letter, one number and one special character' : passwordFocus===false && !validPassword ? 'Password should be 5 to 30 characters with at least one letter, one number and one special character' : '';
      } else {
        // Handle the case if `password` is undefined
      }

    }, [password, validPassword, passwordFocus]) 


    const submitHandler = async (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      if(typeof password === 'string'){ 
        dispatch(userLogin({email, password}))
        console.log('dispatched to login')
        navigate('/login')
      }else{
        console.log('password required')
      }
    }


    return (
      <main className="flex items-center justify-center h-screen bg-gradient-to-r from-white to-sky-400">
        <div className="flex mx-6 w-full justify-center md:justify-between rounded-md item-center">
            <div className="hidden md:flex justify-center align-middle md:w-2/3 ">
               <img src="https://source.unsplash.com/random?wallpapers)" className="rounded-l-lg max-h-screen"/>
               <div className="absolute font-medium font-sans flex mt-52 justify-center text-white">
                <div>
                  <h1 className="text-2xl">Coding reference...</h1>
                  <h1 className="text-4xl">...do it then learn it.</h1>
                </div>
               </div>
               

            </div>
            <div className='flex py-3 rounded-r-md md: rounded-l-md md:w-1/3 border bg-white min-w-fit'>
                <div className="p-6 justify-center mx-auto">
                      {errorObj ? (
                      <div className="pb-1 mb-3 bg-red-300 rounded"> 
                        <ErrorDisplay errorObj={errorObj} />  
                      </div>
                      ) : (
                        <div className="flex justify-center mb-3">
                          <Link to={"/home"}><img src={companyicon} alt="Invoice logo"/></Link>
                        </div>
                      )} 
                      
                      <form action="" method="POST" onSubmit={submitHandler}>
                          <div className="mb-4 flex justify-start" >
                            <i className='far fa-user flex align-baseline border-b-2 p-2 bg-white rounded-l-lg'></i>
                            <input className="w-full p-2 border-b-2 rounded-r-lg focus:outline-none"
                                type='text'
                                id='email' 
                                placeholder='Email' 
                                name="input login name"
                                value={email}
                                ref={emailRef} 
                                autoComplete="off"
                                spellCheck="false" 
                                onChange={(e)=>setEmail(e.target.value)} 
                                aria-invalid={validEmail ? "false" : "true"} 
                                aria-describedby="logEmail"
                                onFocus={()=>setEmailFocus(true)} 
                                onBlur={()=>setEmailFocus(false)} 
                                />
                          </div>

                          <div className="flex justify-start">
                                <i className="fas fa-lock flex align-baseline border-b-2 p-2 bg-white rounded-l-lg"></i>
                                <input className="w-full p-2 border-b-2 rounded-r-lg focus:outline-none"
                                  type="password" 
                                  id="password" 
                                  placeholder="Password"
                                  ref={passwordRef} 
                                  autoComplete="off"
                                  spellCheck="false" 
                                  onChange={(e)=>setPassword(e.target.value)} 
                                  aria-invalid={validPassword ? "false" : "true"} 
                                  aria-describedby="password"
                                  onFocus={()=>setPasswordFocus(true)} 
                                  onBlur={()=>setPasswordFocus(false)} />
                          </div>
                          <p className="text-red-600 max-w-xs" id="password" ref={passwordsp}></p>

                          <div className="mb-4">
                                <small><a className="mt-2 text-blue-700" href="reset-password?sendemail-linkforpassword">Forgot Password?</a></small>
                          </div>
                          
                
                          <div className="flex justify-center mb-3 rounded-lg">
                            <ReCAPTCHA
                                sitekey={process.env.REACT_APP_RECAPTCHA_KEY}
                                onChange={onChange}
                                ref={recaptchaRef}  
                            />                              
                          </div>

                          <div className="flex justify-center">
                            <button className={`w-3/6 inline-block text-white py-1 px-4 rounded mb-3 focus:outline-none ${validEmail && validPassword && recaptchaVerified ? "bg-yellow-600" : "bg-yellow-300"}`} type="submit" disabled={!validEmail || !validPassword || !recaptchaVerified}>Login</button>
                          </div>

                          <div className="mt-3 text-center text-blue-700"> 
                          <Link to={'/signup'}>
                            <span>Not yet a member?&nbsp;</span>
                            Sign up</Link>
                          </div>
                      </form>
                      <GoogleOauth />
                </div>
            </div>
            
        </div>
      </main>
    )
}

export default LoginForm; 