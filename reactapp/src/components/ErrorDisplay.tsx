import React, { useEffect, useState, ReactNode } from 'react';
import { useLimitCharacters} from '../utilities/customHooks'

interface ErrorType {
  errorObj: boolean | string | object | undefined;
}

const ErrorDisplay: React.FC<ErrorType> = ({ errorObj }) => {
  const [errType, setErrType] = useState<string | null>(null);
  const [alreadyExist, setAlreadyExist] = useState<boolean>(false);
  const [tooLong, setTooLong] = useState<boolean>(false);
  const limitCharacters = useLimitCharacters();

  useEffect(() => {
    if (typeof errorObj === 'object' && errorObj !== null) {
      try {
        setErrType(JSON.stringify(errorObj));
      } catch (error) {
        setErrType('Invalid error object');
      }
    } else if (typeof errorObj === 'string') {
      setErrType(errorObj);
    }
  }, [errorObj]);

  useEffect(() => {
    if (errType) {
      const isAlreadyExist = errType.includes("already exists.");
      const isTooLong = errType.includes("value too long for type character");
      
      setAlreadyExist(isAlreadyExist);
      setTooLong(isTooLong);
    }
  }, [errType]);

  return (
    <div className="mt-6 flex justify-center">
      {typeof errorObj === 'string' && errorObj.startsWith('{') && errorObj.endsWith('}') ? (
        <p className="text-center text-red-600">
          {Object.values(JSON.parse(errorObj)).map((value, index) => (
            <span key={index}>{String(value)}</span>
          ))}
        </p>
      ) : (
        <div className="w-1/2 text-center text-red-600">
          <p>
            {alreadyExist ? (
              "This name already exists."
            ) : tooLong ? (
              "Value too long for type character."
            ) : (
              errType && limitCharacters(errType, 180)
            )}
          </p> 
        </div>
      )}
    </div>
  );
  
}
export default ErrorDisplay;

