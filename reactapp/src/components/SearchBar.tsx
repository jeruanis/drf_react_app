
import React, { useState, useEffect, useRef, useCallback} from 'react'
import { selectSearchEdusData, detailSearchContent, searchStatus, resetSearchStatus } from '../features/contents/contentsSlice'
import { useSelector} from 'react-redux';
import { useGetTokenCookie } from '../utilities/cookieAccess';
import { useNavigate } from 'react-router-dom'
import _ from 'lodash';
import { useAppDispatch } from '../app/store';
import { CombinedType } from '../features/contents/contentTypes'

interface ConfigType {
  mainMenuRef: React.RefObject<HTMLButtonElement>;
  profileMenuRef:React.RefObject<HTMLButtonElement>;
}
interface SearchBarProps {
  config: ConfigType;
}

const SearchBar: React.FC<SearchBarProps> = ({config}) => {
  const {mainMenuRef, profileMenuRef} = config;
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const searchResult = useSelector(selectSearchEdusData)
  const status = useSelector(searchStatus)
  const token = useGetTokenCookie('token')
  const searchRef = useRef<HTMLInputElement>(null)
  const [ closeDropdown, setCloseDropdown ] = useState<boolean>(true)
  const [searchTerm, setSearchTerm] = useState<string>('');
  const searchTermRef = useRef(searchTerm);
  const [filteredData, setFilteredData] = useState<CombinedType[]>()


  useEffect(() => {
    searchTermRef.current = searchTerm;
  }, [searchTerm]);

  const debouncedSearch = useRef( 
    _.debounce((searchTerm) => {
      if (searchTerm.trim() !== '') {
        dispatch(detailSearchContent({ token, searchTerm }));
      }
    }, 1000)
  ).current;
  
  const handleDebounce = useCallback(() => {
    const currentSearchTerm = searchTermRef.current;
    debouncedSearch(currentSearchTerm);
  }, [debouncedSearch, searchTermRef]);
  
  useEffect(() => {
    return () => {
      debouncedSearch.cancel();
    };
  }, [debouncedSearch]);


  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(e.target.value)
    handleDebounce();   
    console.log('handleDebounce is executed')
  }

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => { 
    if(e) {e.preventDefault(); e.stopPropagation()}
    if(searchTerm.trim() !== '' ){
      dispatch(detailSearchContent({token, searchTerm}))   
    }  
  }

  
  const handleClick = (item:CombinedType) => {
    if(item.topic_name){
      navigate(`/detail/${item.sub_subject}`, { state: {topicId:item.id, subjectId: item.subjectId}})
    }else if(item.name){
      navigate(`/sub-subject/${item.subject}`)
    }
  }

 
  const slideUp = useCallback(() => {
    setCloseDropdown(true)
    if (searchRef.current) {
      searchRef.current.value = '';
    }
    setSearchTerm('') 
  },[searchRef])


  const slideUpToggle = useCallback((event: MouseEvent) => {
    const target = event.target as Node; // Type assertion
    if (mainMenuRef.current && mainMenuRef.current.contains(target)) {
      // Click inside mainMenuRef
      return;
    }
    if (profileMenuRef.current && profileMenuRef.current.contains(target)) {
      // Click inside mainMenuRef
      return;
    }
  },[mainMenuRef, profileMenuRef])


  useEffect(()=>{ 
    if(status==='succeeded' && searchTerm !== ''){
      setCloseDropdown(false)
      dispatch(resetSearchStatus())
    }

    setFilteredData(searchResult.searchEdusData)
    
  }, [status, dispatch, searchTerm, closeDropdown, filteredData, searchResult.searchEdusData]) 


  useEffect(() => {
    document.addEventListener('click', slideUp)
    return ()=>{
      document.removeEventListener('click', slideUp)
    }
  }, [slideUpToggle, slideUp]) 


  return (
        <>
        <form onSubmit={handleSubmit} className="flex flex-start border-2 rounded-lg lg:w-80">
          <input ref={searchRef} placeholder="Search…" type="text" aria-label="search" className="p-1 pl-3 w-full text-gray-500 text-lg focus:outline-none" value={searchTerm} onChange={handleSearch} />
          <button className="self-center" type="submit">    
            <svg className="w-10 font-md border-l-2 rounded-r-lg bg-purple-400 h-full p-1" focusable="false" aria-hidden="true" viewBox="0 0 24 24" data-testid="SearchIcon"><path fill="white" d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
            </svg>
          </button> 
        </form>  

        <div className={`${closeDropdown ? 'hidden' : ''} absolute overflow-auto h-80 bg-white xs:w-full sm:w-full lg:w-80 border-2`}>
          {filteredData && filteredData.map((item, index) => { 
              return (
                <button key={index} onClick={()=>handleClick(item)} className="text-left block">
                  <div className="p-2 pl-10 cursor-pointer"> 
                    <h3>{item.topic_name || (item.name + ' sub_subject')}</h3>  
                  </div>  
                </button>
              )            
            })} 
        </div>
        </>
  )
}
export default SearchBar;   




