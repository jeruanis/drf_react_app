import React, {ReactNode} from 'react';
import { Link } from 'react-router-dom'


interface configPropType {
  path: string;
}
interface ButtonType {
  addclass:string;
  config: configPropType;
  children: ReactNode;
}

const Button = ({addclass, config, children}:ButtonType) => {

  return (
    <Link to={config.path}>
        <button className={`py-1 px-4 rounded mb-3 ${addclass}`}>
         {children}
        </button>
    </Link>  
  )
}

export default Button; 