

import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useGetTokenCookie } from '../utilities/cookieAccess';
import { addSubSubject, selectHomeEdusData, hasErrorSubSubject, selectSubSubjectData} from '../features/contents/contentsSlice'
import { useNavigate, useParams, useLocation } from 'react-router-dom'
import { useGetSubjectName } from '../utilities/customHooks';
import BackLink from './BackLink' 
import ErrorDisplay from './ErrorDisplay'
import NavComponent from './NavBar'
import { useAppDispatch } from '../app/store';

const AddSubSubjectForm = () => {
  const { id } = useParams();
  const subject = id;
  const [name, setName] = useState<string | undefined>()
  const [subSubjectFocus, setSubSubjectFocus] = useState<boolean | undefined>()
  const [description, setDescription] = useState<string | undefined>() 
  const [descriptionFocus, setDescriptionFocus] = useState<boolean | undefined>()
  const [subjectName, setSubjectName] = useState<string | undefined>()
  const token = useGetTokenCookie('token')
  const selectSsData = useSelector(selectSubSubjectData) 
  const selectHomeData = useSelector(selectHomeEdusData)
  const errorDetail = useSelector(hasErrorSubSubject);
  const dispatch = useAppDispatch();
  const navigate = useNavigate() 

  const getSubjectName = useGetSubjectName()

useEffect(()=>{
   setSubjectName(getSubjectName(selectHomeData.homeEdusData, id))
}, [subjectName, name]) 


  useEffect(()=>{ 
    if(selectSsData.newItem === true){  
      navigate(`/sub-subject/${subject}`)
    }
  },[selectSsData])


  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) =>{
    e.preventDefault()
      dispatch(addSubSubject({name ,subject, token}))
    }

  return (
    <>
    <NavComponent />
    <div className="mt-24 mb-1 container px-4"> 

    <BackLink />

    <p className="text-2xl font-bold mb-6">Adding sub-subject to {subjectName} </p>

      <form action="" method="POST" className="w-full max-w-xl" onSubmit={handleSubmit}>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
              Topic
            </label>
          </div>
          <div className="md:w-2/3">
            <input className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id="subject" type="text" 
            placeholder="Add topic"
            autoComplete='off'
            onChange={(e)=>setName(e.target.value)}
            onFocus={()=>setSubSubjectFocus(true)}
            onBlur={()=>setSubSubjectFocus(false)} 
             />
          </div>
        </div>
  
        <div className="md:flex md:items-center">
          <div className="md:w-1/3"></div>
          <div className="md:w-2/3">
            <button className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
              Add
            </button>
          </div>
        </div>
      </form>
    </div>
    <ErrorDisplay errorObj={errorDetail}/>
    </> 
  )
}

export default AddSubSubjectForm;