
import React from 'react';
import { useSelector } from 'react-redux'
import { isLoadingHome,
          hasErrorHome, 
          isLoadingDetail, 
          hasErrorDetail
        } from '../features/contents/contentsSlice'; 

import { loginIsLoading, loginHasError } from '../features/login/loginSlice'
import { signUpIsLoading, signupHasError } from '../features/signup/signupSlice'

const Spinner = () => {
   const loadingHome = useSelector(isLoadingHome)
   const errorHome = useSelector(hasErrorHome)
   const loadingDetail = useSelector(isLoadingDetail)
   const errorDetail = useSelector(hasErrorDetail) 
   const loadingLogin = useSelector(loginIsLoading)
   const errorLogin = useSelector(loginHasError)
   const loadingSignup = useSelector(signUpIsLoading) 
   const errorSignup = useSelector(signupHasError) 

  return (
    <>
       {(loadingHome || loadingDetail || loadingLogin || loadingSignup ) ? (
            <div className="absolute w-full flex justify-center h-12 items-center mx-auto">
               <div>
                  <div className="border-t-4 border-blue-500 animate-spin rounded-full h-8 w-8 mx-auto"></div>
                  <p className="text-center">Loading content..</p>
               </div>
            </div>
         
      ) : (

         <div className="absolute w-full flex justify-center h-12 items-center mx-auto">
               <div>
                  <div className="border-t-4 border-blue-500 animate-spin rounded-full h-8 w-8 mx-auto"></div>
                  <p className="text-center">Loading content..</p>
               </div>
            </div> 
      ) }

      </>
    
   ) 
  
}

export default Spinner;
