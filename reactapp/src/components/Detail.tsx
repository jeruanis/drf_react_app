import { useParams, useLocation, Link} from 'react-router-dom';
import React, { useEffect, useState, useRef, ChangeEvent, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { selectDetailEdusData, detailGetContent, removeDetailNewItemFlag, saveDetailEdit, deleteTopic, selectSubSubjectData, selectDetailData, getSubSubject } from '../features/contents/contentsSlice'
import { useGetTokenCookie } from '../utilities/cookieAccess';
import { useGetSubSubjectName, useCapitalize, useLimitCharacters, useScrollToBottom, useScrollToTop} from '../utilities/customHooks'
import { Tooltip } from './Tooltip' 

import ContentRenderer from './DataParser'
import  Spinner from './Spinner'
import Button from './Button'
import { EditIcon, ExpandIcon, IconDelete, IconPlus, ArrowDownIcon, ArrowUpIcon, MenuIcon } from './Icons'
import NavComponent from './NavBar'
import { useAppDispatch } from '../app/store';
import {Detail} from '../features/contents/contentTypes'

const DetailComponent = () => {
   const scrollableRef = useRef<HTMLDivElement>(null) 
   const leftHeaderRef = useRef<HTMLHeadElement>(null)
   const location = useLocation()
   const [showComponent, setShowComponent] = useState<boolean | null>() 
   const dispatch = useAppDispatch() 
   const token = useGetTokenCookie('token')
   const capitalize = useCapitalize()
   const limitCharacters = useLimitCharacters() 
   const scrollTop = useScrollToTop()
   const scrollBottom = useScrollToBottom()
   const { id } = useParams();
   const subSubjectId = id ? parseInt(id) : '';

   const topicId = location.state?.topicId;
   const subjectId=location.state?.subjectId;

   console.log('topicId from state: ', topicId)
   console.log('subjectId from search: ', subjectId)

   const [subSubjectName, setSubSubjectName] = useState<string | undefined>()
   const selectDetailContent = useSelector(selectDetailEdusData)
   const selectDetailNewItem = useSelector(selectDetailData)
   const selectSsData = useSelector(selectSubSubjectData) 
   const getSubSubjectName = useGetSubSubjectName() 

   const filteredData = useMemo(()=>{
      return selectDetailContent.filter(value => value.sub_subject === subSubjectId)
   },[subSubjectId, selectDetailContent]);

   const highestChosenTopic = filteredData.reduce((prev, curr) => {
      return (prev.id > curr.id) ? prev : curr;
   }, { id: -Infinity });
   const highestChosenTopicId = highestChosenTopic.id;

   const chosenTopic = useMemo(()=> {
      return filteredData.find(item=>item.id === (topicId || highestChosenTopicId )); 
   },[highestChosenTopicId, filteredData, topicId]);


   const [ selectedItem, setSelectedItem ] = useState<Detail | null>(null)
   const [, setInputValue] = useState<string>('');
   const [ slugArray, setSlugArray ] = useState<Detail[]>([])
   const [isEditMode, setIsEditMode] = useState(false);
   const [isExpanded, setIsExpanded] = useState(false); // By default, it's not expanded
   const [ expandPropLeft ,setExpandPropLeft] = useState<string>()
   const [ expandPropRight, setExpandPropRight] = useState<string>()
   const [showHeader, setShowHeader] = useState<boolean>(false);

   const handleEditClick = (e:any) => {
      setIsEditMode(true)
   };

   const handleTitleChange = (e:ChangeEvent<HTMLInputElement>) => {
      setInputValue(e.target.value);
      setSelectedItem((prev) => {
         if (prev === null) return null;
            return { ...prev, topic_name: e.target.value };
       });
   }

   const handleDataChange =  (e:  React.ChangeEvent<HTMLTextAreaElement>) => {
      setSelectedItem(prevItem => {
         if (prevItem) {
           return { ...prevItem, article: e.target.value };
         }
         return null;
       });
   } 

   const handleDelete = () => {
      if(selectedItem){ 
         const topicId = selectedItem.id
         try{ 
            if(topicId)
               dispatch(deleteTopic({topicId, token})) 
            setSelectedItem(null)
         }catch(e){
            console.error("Error deleting: ", e)
         }
      }
   }

   const handleSave = () => {
         if (selectedItem) {
            const topicId = selectedItem.id;
            const topic_name = selectedItem.topic_name;
            const article = selectedItem.article;

            dispatch(saveDetailEdit({topicId, topic_name, article, token}));
            setIsEditMode(false); 
         }else{
            console.log('cannot execute handleSave as there is no data')
         }
   }

   useEffect(()=>{
      const timer = setTimeout(()=>{ 
         setShowComponent(true)
      }, 300)

      return ()=> clearTimeout(timer)
   }, [showComponent])


   const handleExpand = () => {
      setIsExpanded(prev => !prev);
   };

   const handleScrollUp = () => {
      const elementId = scrollableRef.current
      scrollTop(elementId);  
   } 
   const handleScrollDown = () => {
      const elementId = scrollableRef.current
      scrollBottom(elementId);  
   } 

   const openSideMenu = () => {
      setShowHeader(!showHeader)
   }

   useEffect(() => {
      if (isExpanded) {
         setExpandPropLeft("hidden");
         setExpandPropRight("w-full"); 
      } else {
         setExpandPropLeft("w-full lg:w-2/6");
         setExpandPropRight("w-full lg:w-4/6");
      }
   }, [isExpanded]);


   useEffect(() => {
      let shouldFetchData = false;
         if (selectDetailNewItem?.newItem === true) { 
            if(!isNaN(Number(subSubjectId)) && subSubjectId !== ""){
               dispatch(detailGetContent({token, subSubjectId}));         
               dispatch(removeDetailNewItemFlag()); 
               console.log('update dom')
            }
         } else if (selectDetailContent && Object.keys(selectDetailContent).length > 0) {

            if (filteredData.length === 0) { 
               shouldFetchData = true;
            } else {
               setSlugArray(filteredData); 
            }

            if(chosenTopic){
               if(selectedItem){
                  if(selectedItem.sub_subject === subSubjectId){
                     setSelectedItem(prev=>prev)
                  }else{
                     setSelectedItem(chosenTopic)
                  }
               }else{
                  setSelectedItem(chosenTopic)
               }
            }

         } else {
            shouldFetchData = true; 
         }

         if (shouldFetchData) {
            if(!isNaN(Number(subSubjectId)) && subSubjectId !== ""){
               dispatch(detailGetContent({token, subSubjectId})); 
            }
         }

         if(highestChosenTopicId === -Infinity){
            setSlugArray([])
         }

   }, [dispatch, subSubjectId, selectedItem, selectDetailNewItem?.newItem, topicId, highestChosenTopicId, chosenTopic, filteredData,selectDetailContent, token]); //#vup removed selectDetailContent this make the re-render infinite. 


   useEffect(()=>{
      if ((selectSsData.edusData).length === 0) { 
         console.log('retrieve for subSubject') 
         dispatch(getSubSubject({token, subjectId})); 
      }else{
         console.log('no retrieving happened')
      } 
   }, [dispatch, subSubjectName, selectSsData.edusData.length, subjectId, token])


   useEffect(()=>{
      setSubSubjectName(getSubSubjectName(selectSsData.edusData, id))
     
   }, [subSubjectName, subSubjectId, getSubSubjectName, id, selectSsData.edusData]) 

 console.log('subsubjectname; ', subSubjectName)

  return ( 
         <>
         <NavComponent /> 
         <div className="w-full flex flex-wrap mx-auto px-2 my-10 pt-40 md:pt-16 lg:fixed h-full">
            { showComponent ? ( 
            <>
               <div className={`${expandPropLeft} lg:pl-2 text-xl text-gray-800 leading-normal`} id="resizable">

                  {/* left side header  */}
                  <header className="flex z-10">
                     <MenuIcon onClick={openSideMenu} className="mr-2 inline-block lg:hidden self-center hover:cursor-pointer"/>
                     <p className="text-2xl mb-1 font-bold text-blue-500 inline-block self-center"><Link to={`/sub-subject/${subjectId}`} >{subSubjectName}</Link></p>  
                  </header>

                  <header className={`${showHeader ? "slide-in inline-block absolute w-80 bg-gray-300 border-gray-200 border p-2 rounded z-10" : "hidden"} lg:inline-block lg:w-full`} ref={leftHeaderRef}>
                  <div className="flex justify-between">
                     <p className="text-base font-bold pt-2 text-gray-700">Topics</p>
                     <Button addclass="" config={{path: `/add_topic/${id}`}} >
                        <Tooltip content="Add Topic">
                           <IconPlus />
                        </Tooltip>
                     </Button>
                  </div>

                  {/* Listing topics on the left side */}
                  <div className="h-full w-full inset-0 overflow-x-hidden overflow-y-auto lg:overflow-y-hidden lg:block mt-0 lg:border-gray-400 lg:border-transparent bg-white lg:shadow-lg lg:shadow-indigo-500/40 lg:bg-transparent pb-2 " id="menu-content">
                     <ul className="h-cust55 overflow-auto">

                     {/* if no topic is available yet then show this */}
                     {!selectedItem && (
                        <span className={` block pl-4 align-middle text-gray-900 no-underline border-l-4 border-transparent lg:hover:border-gray-400 font-serif mb-5 md:pb-0 text-lg font-bold hover:bg-purple-500 hover:text-white`}>
                        No Topic available                               
                     </span>  
                     )} 

                     {/* assign value to selected item when topic is clicked */}
                     { (selectedItem || selectDetailNewItem.newItem ||slugArray || highestChosenTopicId ) ?  (slugArray 
                        .sort((a, b) => a.id-b.id) 
                        .map((dict, index) => (
                        <li key={index} className="md:my-0 hover:bg-purple-100 lg:hover:bg-transparent cursor-pointer" 
                        onMouseDown={()=>setSelectedItem(dict)}>
                           <span className={`${ selectedItem && (dict.topic_slug === selectedItem.topic_slug) ? "bg-purple-500 text-white" : "text-lime-700" } block pl-4 align-middle no-underline border-l-4 border-transparent lg:hover:border-gray-400 font-serif mb-5 md:pb-0 text-lg font-bold hover:bg-purple-200 hover:text-gray-500 break-all`}>
                                                                                 
                              {limitCharacters(capitalize("* "+ dict.topic_name ), 30)}                                                                                                  
                           </span>          
                           </li>
                           )
                         ) //map end
                         ): null } 
                     </ul>

                     </div>
                  </header>                 
               </div>

               {/* right side detail */} 
                  
               { isEditMode ?  (
                  
                  <div className={`${expandPropRight} px-3 mt-3 mb-24 pb-24 lg:mt-0 text-gray-900 leading-normal border rounded`}>
                     <form className="relative w-full" >
                        {/* title of right side */}
                        <div className="font-sans">
                           <div className="flex flex-start pb-3">
                              <h1 className="font-medium font-sans break-normal text-gray-500 text-xl pr-2">Title:</h1>
                              <input value={(selectedItem && selectedItem.topic_name) || ''} onChange={handleTitleChange} className="text-xl w-full" />                                                     
                           </div>
                           <hr className="border-b border-gray-300 mb-4" />
                        </div>

                        {/* textarea */}
                        <textarea style={{ whiteSpace: 'pre-line', height: '63vh' }} value={(selectedItem && selectedItem.article) || ''} onChange={handleDataChange} className="w-full outline-none p-3" />
                        
                     </form>
                     <div className="flex justify-between px-10 pt-4 border-t-2 border-t-gray-300">
                        <button className="font-bold bg-purple-500 hover:bg-purple-700 rounded px-4 py-1" onClick={handleSave}>Save</button>

                        <button onClick={()=>setIsEditMode(false)}  className="text-white font-bold bg-gray-700 hover:bg-gray-500 rounded px-4 py-1">Cancel</button>
                     </div>
                  </div>

                  ) : ( 

                  <div ref={scrollableRef} className={`${expandPropRight} px-3 mt-6 mb-24 pb-24 lg:mt-0 text-gray-900 leading-normal bg-white overflow-y-scroll h-screen-90 rightDetail`}>
                     

                     {/* //<!--Title-->  */}
                     <div className="font-sans">
                        <div className="flex justify-between pb-3">
                           <div>
                              <h1 className="font-medium font-sans break-normal text-gray-900 text-xl inline-block">{ selectedItem && selectedItem.topic_name }
                              </h1>
                              <button className="inline-block align-top" onClick={handleDelete}>
                                 {selectedItem ? ( 
                                 <Tooltip content="Delete Topic">
                                    <IconDelete />
                                  </Tooltip>
                                 ) : null }
                              </button> 
                           </div>
                           {selectedItem && (
                              <div className="mr-6 pr-6">
                                 <button onClick={handleEditClick} >
                                 <Tooltip content="Edit Topic"> 
                                    <EditIcon prop=""/>
                                 </Tooltip>
                                 </button> 
                                 <button onClick={handleExpand} className="hidden md:inline-block" >
                                 <Tooltip content="Expand"> 
                                    <ExpandIcon />                          
                                 </Tooltip>
                                 </button>
                                 <button onClick={handleScrollDown} >
                                 <Tooltip content="Down"> 
                                    <ArrowDownIcon />
                                 </Tooltip>
                                 </button>
                              </div>
                              )}
                        </div>
                        <hr className="border-b border-gray-300 mb-4" />
                     </div>

                     {/* //<!--Post Content--> */}
                     <section className="pl-3 py-6 text-lg">
                        <pre style={{ whiteSpace: 'pre-line'}} >  
                           {selectedItem && typeof selectedItem.article === "string" &&(
                              <ContentRenderer content={ selectedItem.article } />
                           )}
                        </pre>   
                     </section>

                     {/* Bottom right side icons */}
                     {selectedItem && typeof selectedItem !== 'string' && selectedItem.article && selectedItem.article.length > 4000 ? (                      
                     <div className="font-sans absolute bottom-10 right-2">
                        <div className="flex justify-end pr-8 pb-3">
                           {selectedItem && (
                              <div className="mr-6 pr-3 bg-white py-1 rounded-md">
                                 <button onClick={handleScrollUp}>
                                 <Tooltip content="Up"> 
                                    <ArrowUpIcon /> 
                                 </Tooltip>
                                 </button>
                                 <button onClick={handleScrollDown} >
                                 <Tooltip content="Down"> 
                                    <ArrowDownIcon />
                                 </Tooltip>
                                 </button>
                                 <button onClick={handleEditClick} >
                                 <Tooltip content="Edit Topic"> 
                                    <EditIcon prop=""/>
                                 </Tooltip>
                                 </button> 
                                 <button onClick={handleExpand} className="hidden md:inline-block" >
                                 <Tooltip content="Expand"> 
                                    <ExpandIcon />                          
                                 </Tooltip>
                              </button>
                              </div>
                              )}
                        </div> 
                     </div>
                     ) : null }

                  </div>
               )}

            </>

            ) : (

              <Spinner />

            ) }

         </div>
      </> 
  ) 
}

export default DetailComponent;
