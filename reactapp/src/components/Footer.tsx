import React from 'react';


const Footer = () => {
  return (
    <div className="w-full bg-gray-900 mt-10">
    <footer className="flex justify-center mt-20 w-full">
      <section style={{padding:'1rem 0', fontSize:'14px'}}>
          <div className="d-flex flex-column text-center text-white" style={{minWidth:'50%'}}>			
            <p className="mb-0 px-2"> &#169; 2019 jba.homes </p>
          </div>
      </section>
    </footer>
    </div>
  )
}

export default Footer;