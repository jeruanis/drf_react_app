import React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

interface ConfigProp {
  index: number;
  title: string;
  description:string;
}
interface BasicAccordionProps {
  config: ConfigProp;
}

export default function BasicAccordion({config}: BasicAccordionProps) {
  return (
    <div>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography>
          <span className="font-sans" key={config.index}><p dangerouslySetInnerHTML={{ __html: config.title}} className="py-3" /></span>;
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            {config.description}
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}