import React, { useState, ReactNode } from 'react'


interface ToolTipType {
  content: string;
  children: ReactNode;
}

export const Tooltip = ({ children, content }:ToolTipType) => {
  const [isVisible, setIsVisible] = useState(false); 

  return (
    <div className="relative inline-block">
      <div
        onMouseEnter={() => setIsVisible(true)}
        onMouseLeave={() => setIsVisible(false)}
      >
        {children}
      </div>
      {isVisible && (
        <div
          className="absolute z-10 mt-2 p-2 bg-gray-800 text-white text-sm rounded-md shadow-lg"
          style={{ minWidth: '100px' }}
        >
          {content}
        </div>
      )}
    </div>
  );
}