import React, { useState, useEffect, Fragment } from 'react'
import { selectHomeEdusData, homeGetContents } from '../features/contents/contentsSlice'
import { useSelector, useDispatch } from 'react-redux'
import { Menu, Transition } from '@headlessui/react'
import { Link, useNavigate } from 'react-router-dom'
import { useGetTokenCookie } from '../utilities/cookieAccess';
import { useAppDispatch } from '../app/store'


function classNames(...classes:string[]) {
  return classes.filter(Boolean).join(' ') 
}

const CategoryDropdown = () => {
  const courseList = useSelector(selectHomeEdusData)
  const dispatch = useAppDispatch()
  const token = useGetTokenCookie('token') 
  const navigate = useNavigate()


  useEffect(() => {
    let shouldFetchData = Object.keys(courseList).length > 0 ? true : false;
   
    if (shouldFetchData) {
      if(typeof token === 'string'){
            dispatch(homeGetContents(token))
      }
    }
  }, [dispatch]); 


  const handleClick = (subjectId:number|undefined, subjectName:string) => { 
    navigate(`/sub-subject/${subjectId}`, {state: { subjectNamefromCat: subjectName }})
  }

  return (

      <Menu as="div" className="relative text-gray-600 rounded-md px-3 py-2 text-sm font-medium">
      <div>
        <Menu.Button>
          Category
        </Menu.Button>
      </div> 

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95" 
      >

        <Menu.Items className="absolute right-0 z-10 mt-2 w-72 min-w-max origin-top-right bg-gray-100 focus:outline-none">
          <div className="flex flex-col item-center">
            <div className="w-auto bg-light overflow-y-auto h-96 overflow-x-hidden">
              { courseList && [...courseList.homeEdusData].sort((a,b)=>a.subject_name.localeCompare(b.subject_name)).map((dict, index) =>{
                return (
                  <Menu.Item key={index}>
                    {({ active }) => (
                          <button onClick={()=>handleClick(dict.id, dict.subject_name)} 
                            className={`${classNames(
                              active ? 'bg-purple-500 text-gray-900 block text-center pl-4 py-2 w-auto' : 'text-gray-700 block pl-4 py-2 w-auto'
                            )} w-full`}
                          >
                            <p className="text-md font-serif text-lg font-medium text-gray-500 bg-white hover:bg-purple-500 hover:text-white">{dict.subject_name}</p> 
                          </button>
                    )}
                  </Menu.Item>   
                )
              })} 
            </div>
          </div>
        </Menu.Items>
      </Transition>
    </Menu>

    )
}
export default CategoryDropdown;   
