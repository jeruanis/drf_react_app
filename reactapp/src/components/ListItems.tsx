
/**
 * 
 * @param {slugArray, selectedItems} props 
 * @returns Jsx List
 */

import React, { useState, useEffect, useMemo} from 'react'
import { useSelector } from 'react-redux'
import { useCapitalize } from '../utilities/customHooks'
import { selectDetailEdusData, detailGetContent, hasErrorDetail, isLoadingDetail} from '../features/contents/contentsSlice'
import {useNavigate } from 'react-router-dom'
import Spinner from './Spinner'
import { useAppDispatch } from '../app/store'
import { Detail } from '../features/contents/contentTypes'

interface ListenType {
    subSubjectId: string | number;
    subjectId: string | number;
    token: string;
}
const ListItems:React.FC<ListenType> = (props) => {

  const {
    subSubjectId,
    subjectId,
    token,
  } = props;


  const capitalize = useCapitalize()
  const detailData = useSelector(selectDetailEdusData) 
  const isLoading = useSelector(isLoadingDetail)
  const [ itemsSelected, setItemsSelected] = useState<Detail[]>()
  const hasError = useSelector(hasErrorDetail)
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const isTopicExisting = Object.values(detailData).find(dict=>{
    const condition = dict.sub_subject === subSubjectId;
    return condition;
   }) 


  const filteredTopics = useMemo(() => {
    return Object.values(detailData).filter(dict => dict.sub_subject === subSubjectId);
  }, [detailData, subSubjectId]); 

  
  console.log('this is fileteredTopics', filteredTopics)
  useEffect(()=>{
    if(!isNaN(Number(subSubjectId)) && subSubjectId !== ""){
      dispatch(detailGetContent({token, subSubjectId}));
    }
  }, [dispatch, subSubjectId, token])//


  useEffect(()=>{ 
      if(isTopicExisting){ 
        console.log('isTopic is existing')
        setItemsSelected(filteredTopics)
      }else{
        setItemsSelected([{id:0, topic_name:'No Content add some.', topic_slug:'no-cont', article:'', created_at:'', sub_subject:0}])
      }

  },[filteredTopics, isTopicExisting])


  const handleClick = (dictId:number) => {
    navigate(`/detail/${subSubjectId}`, {state: { topicId: dictId, subjectId:subjectId }})
  }


  if(hasError){
    return <p>Has Error Encountered</p>

  }else if(isLoading){
    return <Spinner /> 
  } 
 
  return (
    
    <ul className="list-reset">

    {itemsSelected ? (Object.values(itemsSelected) 
      .sort((a, b) => a.id-b.id)
      .map((dict, index) => (  
      <li key={index} className="md:my-0 hover:bg-purple-100 lg:hover:bg-transparent cursor-pointer">

        <button onClick={()=>handleClick(dict.id)}> 
          <span className={`block px-4 align-middle no-underline lg:hover:border-gray-400 font-serif mb-5 md:pb-0 text-lg font-bold hover:bg-purple-500 hover:text-white break-all`}>
                                                            
            {capitalize(dict.topic_name)}
          </span> 
        </button>
      </li>
      )
      )

    ): (
      <span className={` block pl-4 align-middle text-gray-900 no-underline border-l-4 border-transparent lg:hover:border-gray-400 font-serif mb-5 md:pb-0 text-lg font-bold hover:bg-purple-500 hover:text-white`}>
        No Topic available                               
      </span> 
    )} 
    
    </ul>
  )

}

export default ListItems; 
