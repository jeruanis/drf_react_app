

export const useGetSubjectName = () => {
  return ((storelocation:object, id:(string | undefined))=>{
    if(storelocation && Object.keys(storelocation).length > 0){
      for (const [, value] of Object.entries(storelocation)){
        if(id){
          if(value.id === parseInt(id)){
              return value.subject_name
          }
        }
      }
    }
  })
}

export const useGetSubSubjectName = () => {
  return ((storelocation:object, id:(string | undefined))=>{
    console.log('storelocation and Id:', storelocation, id)
    if(storelocation && Object.keys(storelocation).length > 0){
      for (const [, value] of Object.entries(storelocation)){
        if(id){
          if(value.id === parseInt(id)){
              return value.name
          }
        }
      }
    }
  })
}


export const useLimitCharacters = () => { 
  return((input:string, length:number)=>{
    if (input.length <= length) {
      return input;
    }

    const limited = input.substring(0, length);
    const lastIndex = limited.lastIndexOf(" ");
    return `${limited.substring(0, lastIndex)} ...`;
  })  
} 


export const useCapitalize = () => {
  return ((string:string)=> {
    return string.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ')
     })
}
 

export const useScrollToBottom = () => {
  return (element:(HTMLDivElement | null)) => {
    if (element) {
      element.scrollTo({
            top: element.scrollHeight,
            behavior: 'smooth'
        });
    } else {
      console.error(`Element with id ${element} not found`);
    }
  }
}

export const useScrollToTop = () => { 
  return (element:(HTMLDivElement | null))=>{
    if (element) {
      element.scrollTo({
          top: 0,
          behavior: 'smooth'
      });
    } else {
      console.error(`Element with id ${element} not found`);
    }
  } 
}




