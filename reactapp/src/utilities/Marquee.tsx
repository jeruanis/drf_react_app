import React, { useEffect, useRef } from 'react';

interface MarqueType {
  text: string;
  speed: number;
}
const Marquee = ({ text, speed }: MarqueType) => {
  const marqueeRef = useRef<HTMLDivElement | null >(null);

  useEffect(() => {
    if (marqueeRef.current) {
      const marqueeSpan = marqueeRef.current.querySelector('span');
      if (marqueeSpan) {
        const spanWidth = marqueeSpan.getBoundingClientRect().width;
        const time = spanWidth / speed;
        marqueeSpan.style.animationDuration = `${time}s`;
      }
    }
  }, [speed, text]);

  return (
    <div className="marquee" ref={marqueeRef}>
      <span>{text}</span>
    </div>
  );
};

export default Marquee;
