import { string } from "prop-types";

export interface UserSignupInputType {
  name?: string;
  lastName?: string;
  email?: string;
  confirmEmail?: string; 
  pwd?: string;
  matchPwd?: string;
}

export interface UserSignupResultType {
  status: string;
  username: string;
  token: string;
  google: string;
  imageUrl: string;
  email: string;
}

export interface UserLoginGoogleInputType {
  uniqid: string;
  first_name: string;
  last_name: string;
  profile_image: string;
  email: string;
}


export interface UserSignupContentType {
    userData?: UserSignupResultType;
    isLoading: boolean;
    hasError?: boolean | string;
    token:string;
    isLoadingToken:boolean;
    hasErrorToken?:boolean | string;
    isLoadingLoginGoogle: boolean;
    hasErrorLoadingLoginGoogle?: boolean | string;
}

