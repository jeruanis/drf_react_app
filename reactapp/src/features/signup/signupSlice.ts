import React from 'react';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from '../../api/axios';
import { RootState } from '../../app/store'
import { 
   UserSignupInputType,
   UserLoginGoogleInputType,
   UserSignupResultType,
   UserSignupContentType,
} from './signupType'

function getCookie(name: string):string | null {
   let cookieValue = null;
   if (document.cookie && document.cookie !== '') {
       const cookies = document.cookie.split(';');
       for (let i = 0; i < cookies.length; i++) {
           const cookie = cookies[i].trim();
           if (cookie.substring(0, name.length + 1) === (name + '=')) {
               cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
               break;
           }
       }
   }
   return cookieValue;
}


export const userSignup = createAsyncThunk(
   'user/userSignup/:', async ({name, lastName, email, confirmEmail, pwd, matchPwd}: UserSignupInputType)=> {
      const csrfToken = getCookie('csrftoken');
      axios.defaults.headers.post['X-CSRFToken'] = csrfToken;

      try{
         const response = await axios.post<UserSignupResultType>(`account/signup/`, {name, lastName, email, confirmEmail, pwd, matchPwd}, {
         headers: {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-CSRFToken': csrfToken}, withCredentials: true,
         });

         try{
            if(response.status === 200){
                const data: UserSignupResultType= response.data;
                return data;
            } else {
              throw new Error('Request failed with status: ' + JSON.stringify(response))
            }
          }catch(e: any){
            throw new Error('There was an error: ', e)
          }

         }catch(e: any) {
            throw new Error(e.response.request.responseText)
         } 
         
          
   }
)


export const userLoginGoogle = createAsyncThunk(
   'user/userLoginGoogle', async ({uniqid, first_name, last_name, profile_image, email}: UserLoginGoogleInputType) => {

      const csrfToken = getCookie('csrftoken');
      axios.defaults.headers.post['X-CSRFToken'] = csrfToken;

      try {
         const response = await axios.post<UserSignupResultType>(`account/signup/`,  {uniqid, first_name, last_name, profile_image, email}, {
         headers: {'Accept': 'application/json', 'Content-Type': 'application/json', 'X-CSRFToken': csrfToken}, withCredentials: true,
      }); 

         try{
            if(response.status === 200){
               const data: UserSignupResultType = response.data;
               return data;
            } else {
            console.log('Request failed with status: ', response)
            throw new Error('Request failed with status: ' + JSON.stringify(response))
            }
         }catch(e: any){
            throw new Error('There was an error: ', e)
         }

      }catch(e: any) {
         throw new Error(e.response.request.responseText)
      } 
   }
)

const initialState: UserSignupContentType = {
   userData: undefined,
   isLoading: false,
   hasError: false,
   token:'',
   isLoadingToken:false,
   hasErrorToken:false,
   isLoadingLoginGoogle:false,
   hasErrorLoadingLoginGoogle:false,
}


export const signupSlice = createSlice({
   name: 'userSignup',
   initialState,
   reducers:{
      clearUsernameSignup: (state) => {
         state.userData = undefined;
       }
   },
   extraReducers: (builder) => {
      builder
      .addCase(userSignup.pending, (state)=>{
         state.isLoading = true;
         state.hasError = false;
      })
      .addCase(userSignup.fulfilled, (state, action)=> {
         state.userData = action.payload;
         state.isLoading = false;
         state.hasError = false;
      })
      .addCase(userSignup.rejected, (state, action)=>{
         state.isLoading=false;
         state.hasError = action.error.message;
         state.userData=undefined
      })

      .addCase(userLoginGoogle.pending, (state)=>{
         state.isLoadingLoginGoogle = true;
         state.hasErrorLoadingLoginGoogle = false;
      })
      .addCase(userLoginGoogle.fulfilled, (state, action)=>{
         state.userData = action.payload;
         state.isLoadingLoginGoogle = false;
         state.hasErrorLoadingLoginGoogle = false;
      })
      .addCase(userLoginGoogle.rejected, (state, action)=>{
         state.isLoadingLoginGoogle = false;
         state.hasErrorLoadingLoginGoogle = action.error.message;
      })
   }

})


export const selectCurrentUser = (state: RootState) => state.userSignup.userData;
export const signUpIsLoading = (state: RootState) => state.userSignup.isLoading;
export const signupHasError = (state: RootState) => state.userSignup.hasError;
export const { clearUsernameSignup } = signupSlice.actions;
export default signupSlice.reducer;
