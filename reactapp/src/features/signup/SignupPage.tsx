import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useSetTokenCookie, useGetTokenCookie } from '../../utilities/cookieAccess';

import { selectCurrentUserLogin } from '../login/loginSlice';
import {
   selectCurrentUser, 
   signUpIsLoading, 
   signupHasError } from './signupSlice'

import SignupForm from '../../components/SignupForm'


const SignupPage = () => {
   const navigate = useNavigate();
   const isSignUpLoading = useSelector(signUpIsLoading);
   const isSignUpHasError = useSelector(signupHasError)
   const loginUserData = useSelector(selectCurrentUserLogin)
   const signupResult = useSelector(selectCurrentUser);
   const localTokenCookie = useGetTokenCookie('token');
   const existingTokenCookie = signupResult ? signupResult.token : localTokenCookie ? localTokenCookie : '';

   const setCookie =  useSetTokenCookie();
   setCookie({name:'token', value: existingTokenCookie, expirationDays:7 });


   useEffect(()=>{
      if (loginUserData) {
         if(loginUserData.hasOwnProperty('google')){
            navigate('/home') 
         }

         if(loginUserData.hasOwnProperty('username') && loginUserData.hasOwnProperty('imageUrl') && loginUserData.hasOwnProperty('tohome')){
            navigate('/home')
         }
      } 

   },[loginUserData])


   useEffect(()=>{
      if (signupResult) {
         if(signupResult.hasOwnProperty('google')){
            navigate('/home')
         }

         if(signupResult.hasOwnProperty('gologin')){
            navigate('/login')
         }
         
       } else if (typeof signupResult === 'undefined') {
            console.log('signupResult is undefined')
       } else {
         // 
       }

   },[signupResult])

   
   return (
      <>
         <SignupForm />
      </>
   )
} 
export default SignupPage;

