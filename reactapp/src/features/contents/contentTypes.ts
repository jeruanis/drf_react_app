import { String } from "lodash";

export interface Subject {
  id?: number;
  subject_slug: string;
  subject_name: string;
  description: string;
  created_at: string;
}

export interface SubSubject {
  id : number;
  name : string;
  slug : string;
  description : string;
  created_at : string;
  subject : number;
}

export interface Detail {
  id: number;
  topic_name : string;
  topic_slug: string;
  article: string;
  created_at: string; 
  sub_subject : number | string;
}

export interface CombinedType {
  id: number;
  subject_name?:string;
  name?: string;
  slug?:string;
  topic_name?: string;
  topic_slug?: string;
  article?: string;
  description?:string;
  created_at: string; 
  sub_subject?: number | string;
  subject?:number | string;
  subjectId?:number | string;
}

interface TokenType {
  token: string | null;
}

export interface HomeAddContentTypes extends TokenType{
  subject_name:string;
  description:string; 
}

export interface HomeEditContentTypes extends TokenType {
  subjectId?: number;
  subject_name: string;
  description?: string;
}

export interface AddSubSubject extends TokenType {
  name?: string;
  subject?: string;
}

export interface GetSubSubject extends TokenType {
  subjectId:number | string;
}

export interface DetailGetContent extends TokenType {
  subSubjectId: string | number;
}

export interface DetailSearchContent extends TokenType {
  searchTerm: string;
}

export interface DetailAddTopic extends TokenType {
  topic_name?: string;
  article?: string;
  sub_subject?: string;
}

export interface SaveDetailEdit extends TokenType {
  topicId?: number;
  topic_name?: string;
  article?: string;
}

export interface DeleteTopic extends TokenType {
  topicId: number;
}


interface HomeData {
  homeEdusData: Subject[];
  isLoading: boolean;
  hasError?: boolean | string | object;
  newItem: boolean;
}
interface SubSubjectData {
  edusData: SubSubject[];
  isLoading: boolean;
  hasError: string | boolean;
  newItem: boolean;
}
interface DetailData {
  detailEdusData: Detail[];
  isLoading: boolean;
  hasError: string | boolean;
  newItem: boolean;
}
interface SelectedTopics {
  topics: Detail[];
  isLoading:boolean;
  hasError:boolean;
} 
interface SearchData {
  searchEdusData:CombinedType[]; 
  status:string;
  hasError?: any;
}

export interface ContentState {
  homeData: HomeData;
  subSubjectData: SubSubjectData;
  detailData: DetailData;
  selectedTopics: SelectedTopics;
  searchData: SearchData;
}


export interface DeleteResult {
  id: number;
}