import React, { useEffect } from 'react';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from '../../api/axios'
import { RootState } from '../../app/store';

type userLogoutType = RootState['userLogout'];
interface logoutUserData {
  detail: string;
}
interface LogoutStateType {
  userData: logoutUserData;
  isLoading: boolean;
  hasError: boolean;
}

const initialState: LogoutStateType =  {
  userData: {detail: ''},
  isLoading: false,
  hasError: false,
}

export const userLogout = createAsyncThunk(
  'user/logout', async (tokenCookie:string)=> {

    const response = await axios.get<logoutUserData>('account/logout/', {
      headers: {
        'Authorization' : `Token ${tokenCookie}`
      }
    })

    try{
      if(response.status === 200) {
        try {
          const data : logoutUserData =  response.data;
          return data;
        }catch (e){
          const err = await response.data;
          return err;
        }
      }else {
        throw new Error('Request failed');
      }
    }catch(e){
      throw new Error('There was an error: '+ JSON.stringify(e))
    }
  }
)


export const logoutSlice = createSlice({
  name: 'userLogout', 
  initialState, 
  reducers: {},
  extraReducers: (builder) => {
    builder
    .addCase(userLogout.pending, (state)=>{
      state.isLoading = true;
      state.hasError = false;
    })
    .addCase(userLogout.fulfilled, (state, action)=>{
      state.userData = action.payload;
      state.isLoading = false;
      state.hasError = false;
    })
    .addCase(userLogout.rejected, (state)=> {
      state.isLoading = false;
      state.hasError = true;
    })
  }
})

export const selectUserLogout = (state:userLogoutType) => state.userData;
export default logoutSlice.reducer; 
