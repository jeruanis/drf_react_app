import React, { useEffect, useState } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { clearUsernameSignup } from '../signup/signupSlice';
import { clearUsernameLogin } from '../login/loginSlice';
import { userLogout, selectUserLogout } from './logoutSlice';
import { useRemoveCookie, useGetTokenCookie } from '../../utilities/cookieAccess'; 
import { useAppDispatch } from '../../app/store';


const Logout = () => {
   const navigate = useNavigate();
   const dispatch = useAppDispatch();

   const tokenCookie = useGetTokenCookie('token'); 

   useEffect(()=>{
      if(tokenCookie)
         dispatch(userLogout(tokenCookie)) 
   })

   const selectLogout = useSelector(selectUserLogout); 

   const removeLocalToken = useRemoveCookie()
   
   useEffect(()=>{
         if(selectLogout && selectLogout?.detail == 'logoutSuccess'){
            navigate('/login?logout=success')
            removeLocalToken('token');
      } else {
         console.log('No value in logout user data')
         removeLocalToken('token');
         navigate('/login?logout=success')
       
      }
  
   },[navigate, selectLogout])

   useEffect(()=>{
      dispatch(clearUsernameLogin());
      dispatch(clearUsernameSignup());

   },[dispatch])
   
   return(
      <>
         <p>Logout Page</p>
      </>
   )
}
export default Logout;