interface TokenType {
  token: string;
}

export interface UserLoginInputType {
  email: string;
  password: string;
}

interface HasError {
  hasError: boolean;
}
interface IsLoading {
  isLoading: boolean;
}

export interface UserLoginResultType {
  token?: string;
  username: string;
  imageUrl: string;
  tohome?: string;
}

export interface LoginContentState {
  userData? : UserLoginResultType;
  isLoading: boolean;
  hasError?: boolean | string;
}




