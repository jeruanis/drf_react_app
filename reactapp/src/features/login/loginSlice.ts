import React, { useEffect } from 'react';
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from '../../api/axios';
import { RootState } from '../../app/store'
import {
   LoginContentState,
   UserLoginInputType,
   UserLoginResultType
} from './loginType'


type UserLoginStateType = RootState['userLogin'];

function getCookie(name: string):string | null {
   let cookieValue = null;
   if (document.cookie && document.cookie !== '') {
       const cookies = document.cookie.split(';');
       for (let i = 0; i < cookies.length; i++) {
           const cookie = cookies[i].trim();
           if (cookie.substring(0, name.length + 1) === (name + '=')) {
               cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
               break;
           }
       }
   }
   return cookieValue;
}


export const userLogin = createAsyncThunk(
   'user/userlogin', async ({email, password}: UserLoginInputType)=> {

      const csrfToken = getCookie('csrftoken');
      axios.defaults.headers.post['X-CSRFToken'] = csrfToken;

      try{
         const response = await axios.post<UserLoginResultType>('account/login/',  {email, password}, {
            headers: {'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRFToken': csrfToken,
            },withCredentials: true,
         });

         try{
            if(response.status === 200){
                const data:UserLoginResultType = response.data;
                return data;
            } else {
              console.log('Request failed with status: ', response)
              throw new Error('Request failed with status: ' + JSON.stringify(response))
            }
          }catch(e:any){
            console.log('There was an error: ', e)
            throw new Error('There was an error: ', e)
          }

      }catch(e:any) {
         console.log('There was an error1: ', typeof e.response.request.responseText)
         throw new Error(e.response.request.responseText)
     } 
          
   }
)

export const userKeepLogin = createAsyncThunk(
   'user/userloginkeep', async (tokenCookie: string) => {

      try{
      const response = await axios.get<UserLoginResultType>('account/loginkeep/', {
         headers: {
            "Authorization" : `Token ${tokenCookie}`
         }
      });
      
         try{
            if(response.status === 200){
               const data: UserLoginResultType = response.data;
               return data;
            } else {
            console.log('Request failed with status: ', response)
            throw new Error('Request failed with status: '+ JSON.stringify(response))
            }
         }catch(e:any){
            console.log('There was an error: ', e)
            throw new Error('There was an error: ', e)
         }

      }catch(e:any) {
         console.log('There was an error1: ', typeof e.response.request.responseText)
         throw new Error(e.response.request.responseText)
     } 
   }
)

const initialState: LoginContentState = {
   userData: undefined,
   isLoading: false,
   hasError:false
}


export const loginSlice = createSlice({
   name: 'userLogin',
   initialState,
   reducers: { 
      setUsernameLogin: (state, action) => {
        state.userData = action.payload;
      },
      clearUsernameLogin: (state) => {
        state.userData = undefined;
      },

      clearToHome: (state) => {
         delete state.userData?.tohome;
      }

   },
   extraReducers: (builder) => {
      builder
      .addCase(userLogin.pending, (state: UserLoginStateType,)=>{
         state.isLoading = true;
         state.hasError = false;
      })
      .addCase(userLogin.fulfilled, (state: UserLoginStateType, action)=> {
         state.userData = action.payload;
         state.isLoading = false;
         state.hasError = false;
      })
      .addCase(userLogin.rejected, (state: UserLoginStateType, action)=>{
         state.isLoading=false;
         state.hasError=action.error.message;
         state.userData=undefined;
      })
      .addCase(userKeepLogin.pending, (state: UserLoginStateType,)=>{
         state.isLoading=true;
         state.hasError=false;
      })
      .addCase(userKeepLogin.fulfilled, (state: UserLoginStateType, action)=>{
         state.userData=action.payload; 
         state.isLoading=false;
         state.hasError=false;
      })
      .addCase(userKeepLogin.rejected, (state: UserLoginStateType, action)=>{
         state.isLoading=false;
         state.hasError=action.error.message;
         state.userData=undefined;
      })
   }

})


export const selectCurrentUserLogin = (state: RootState) => state.userLogin.userData;
export const loginIsLoading = (state: RootState) => state.userLogin.isLoading;
export const loginHasError = (state:RootState) => state.userLogin.hasError;

export const { 
   setUsernameLogin, 
   clearUsernameLogin,
   clearToHome
 } = loginSlice.actions;

export default loginSlice.reducer;
