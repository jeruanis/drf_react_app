import React, { useEffect} from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useGetTokenCookie, useSetTokenCookie } from '../../utilities/cookieAccess';
import LoginForm from '../../components/LoginForm'
import {
   selectCurrentUser,
   signUpIsLoading,
   signupHasError } from '../signup/signupSlice'
import { 
   selectCurrentUserLogin,
   loginIsLoading,
   loginHasError,
} from './loginSlice'


const LoginPage = () => {
   const navigate = useNavigate()
   const dispatch = useDispatch()

   const hasErrorLoading = useSelector(loginHasError);
   const isSignUpLoading = useSelector(signUpIsLoading);
   const isLoginIsLoading = useSelector(loginIsLoading);

   const localTokenCookie = useGetTokenCookie('token');
   const signupUserData = useSelector(selectCurrentUser);
   const loginUserData = useSelector(selectCurrentUserLogin)

   const existingTokenCookie = (loginUserData?.token) ? loginUserData.token : (localTokenCookie) ? localTokenCookie : (signupUserData?.token) ? signupUserData.token: '';
   const existingImageCookie = loginUserData?.imageUrl ?? localTokenCookie ?? signupUserData?.imageUrl ?? '';

   const setTokenCookie = useSetTokenCookie();
   const setImageCookie = useSetTokenCookie();
   const token = useGetTokenCookie('token')

   const location = useLocation() 
   const queryParams = new URLSearchParams(location.search)
   const logoutParam = queryParams.get('logout')


   useEffect(()=>{
      if (loginUserData && !logoutParam) {
         if( loginUserData.hasOwnProperty('google') || (loginUserData.username && loginUserData.imageUrl && loginUserData.tohome) ) {
            console.log('correct')
            if(existingTokenCookie && existingImageCookie) { 
               setTokenCookie({name:'token', value: existingTokenCookie, expirationDays:7 });
               setImageCookie({name:'imageUrl', value: existingImageCookie, expirationDays:7 });
               navigate('/home')
               return
            } else {
               console.log('There was an error saving token')
            }
         } 
      } 

   },[loginUserData, navigate, logoutParam, dispatch])     


   useEffect(()=>{
      if(signupUserData && !logoutParam){
         if( signupUserData?.google ) { 
            if(existingTokenCookie){ 
               setTokenCookie({name:'token', value: existingTokenCookie, expirationDays:7 });
               setImageCookie({name:'token', value: existingImageCookie, expirationDays:7 });
               navigate('/home') 
               return
            }else{
               console.log('There was an error saving token')
            }
            return;
         } 
      } 
   }, [signupUserData, navigate, logoutParam]) 

   { }
   return (
         <LoginForm/>
   )
} 
export default LoginPage; 


